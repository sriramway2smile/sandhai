
DELETE FROM public."AgriApi_tempverify"
where temp_crop_id_id IN
    (
      SELECT d.id from public."AgriApi_tempcropimage" as d JOIN  public."AgriApi_cropregistration" as c ON d.crop_id_id=c.id JOIN public."AgriApi_landregistration" as b ON b.id=c.land_id_id
      JOIN public."AgriApi_farmer" as a ON a.id = b.farmer_id_id where a.id=97
    )
RETURNING temp_crop_id_id;

DELETE FROM public."AgriApi_tempcropimage"
where crop_id_id IN
    (
      SELECT c.id from public."AgriApi_cropregistration" as c JOIN public."AgriApi_landregistration" as b ON b.id=c.land_id_id
      JOIN public."AgriApi_farmer" as a ON a.id = b.farmer_id_id where a.id=97
    )
RETURNING crop_id_id;

DELETE FROM public."AgriApi_verifiedcrop"
where crop_id_id IN
    (
      SELECT c.id from public."AgriApi_cropregistration" as c JOIN public."AgriApi_landregistration" as b ON b.id=c.land_id_id
      JOIN public."AgriApi_farmer" as a ON a.id = b.farmer_id_id where a.id=97
    )
RETURNING crop_id_id;


DELETE FROM public."AgriApi_cropregistration"
where land_id_id IN
(
	SELECT b.id from public."AgriApi_landregistration" as b JOIN public."AgriApi_farmer" as a ON a.id = b.farmer_id_id where a.id=97
)
RETURNING land_id_id ;

DELETE FROM public."AgriApi_landimage"
where land_id_id IN
(
	SELECT b.id from public."AgriApi_landregistration" as b JOIN public."AgriApi_farmer" as a ON a.id = b.farmer_id_id where a.id=97
)
RETURNING land_id_id ;

DELETE FROM public."AgriApi_landregistration"
where farmer_id_id IN
(
	SELECT id from public."AgriApi_farmer" as a where a.id=97
)
RETURNING farmer_id_id;

DELETE FROM public."AgriApi_usercontact"
where farmer_id_id IN
(
	SELECT id from public."AgriApi_farmer" as a where a.id=97
)
RETURNING farmer_id_id;

DELETE FROM public."AgriApi_farmer" where id=97
RETURNING id ;



