from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from datetime import datetime
from django.conf import settings
from django.forms import ModelForm
from django.utils.translation import ugettext_lazy as _
from versatileimagefield.fields import VersatileImageField, PPOIField

import geocoder
GENDER = (
    ('MALE','Male'),
    ('FEMALE','Female'),
    ('OTHER', 'Other'),
)
DEVICE_TYPE = (
    ('WEB','Web'),
    ('ANDROID','Android'),
    ('IOS', 'IOS'),
)

TICKET_TYPES = (
    ('PAID','Paid Ticket'),
    ('FREE', 'Free ticket'),
    ('BOX_OFFICE','Box Office')
)

TICKET_STATUS = (
    ('WAITING','Waiting'),
    ('PURCHASED','Purchased'),
    ('ATTENDED', 'Attended'),
    ('CANCELLED','Cancelled')
)

def get_geo_info(address):
    g = geocoder.google(address)
    return g;

class Upload(models.Model):
    ImageField = VersatileImageField(
        'Images',
        upload_to='UploadImages/',
        ppoi_field='ImageFieldPPOI'
    )
    ImageFieldPPOI = PPOIField()
    upload_date=models.DateTimeField(auto_now_add =True)

class BaseModel(models.Model):
   created_on = models.DateTimeField(auto_now_add=True,null=True, blank=True)
   modified_on = models.DateTimeField(auto_now=True,null=True, blank=True)
   is_active = models.BooleanField(default=True)
   deleted_at = models.DateTimeField(blank=True, null=True)
   updated_by=models.ForeignKey(settings.AUTH_USER_MODEL,related_name='%(class)s_updated_by', null=True, blank=True)
   created_by = models.ForeignKey(settings.AUTH_USER_MODEL,related_name='%(class)s_createdby', null=True, blank=True)
   class Meta:
       abstract = True


class Locations(BaseModel):
    building_no = models.CharField(max_length=1500, null=True, blank=True)
    address = models.CharField(max_length=1500, null=True, blank=True)
    city = models.CharField(max_length=1500, null=False, blank=True)
    country = models.CharField(max_length=1500, null=False, blank=True)
    land_mark = models.CharField(max_length=1500, null=True, blank=True)
    description = models.CharField(max_length=1500, null=True, blank=True)
    postcode = models.CharField(max_length=1500, null=True, blank=True)
    lat = models.CharField(max_length=1500, null=False, blank=True)
    lon = models.CharField(max_length=1500, null=False, blank=True)
    def save(self,force_insert=False, force_update=False, using=None, update_fields=None):
        if force_insert or self._state.adding:
            if self.address:
                g = get_geo_info(self.address);
                if not self.building_no and g.housenumber:
                    self.building_no = g.housenumber;

                if not self.city and g.city:
                    self.city = g.city;

                if not self.country and g.country:
                    self.country = g.country;

                if not self.postcode and g.postal:
                    self.postcode = g.postal;
                geo = g.latlng;
                if geo:
                    self.lat = geo[0];
                    self.lon = geo[1];
        return super(Locations, self).save()

class ApiClientInfo(BaseModel):
    unique_token = models.CharField(max_length=500, null=False, blank=False)
    ip = models.GenericIPAddressField(null=False,blank=False)
    device_type = models.CharField(max_length=500,choices=DEVICE_TYPE, default='WEB')
    user_agent = models.CharField(max_length=500, null=False, blank=False)
    timezone = models.CharField(max_length=500, null=False, blank=False)
    country = models.CharField(max_length=500, null=False, blank=False)

class AuthToken(BaseModel):
    user = models.ForeignKey(User, related_name='api_user', null=False)
    client_info = models.ForeignKey(ApiClientInfo, related_name='api_client_info', null=False)
    apiKey = models.CharField(max_length=1500, null=False, blank=True)
    last_used_ts = models.DateTimeField(default=timezone.now)

class Roles(BaseModel):
    role_name = models.CharField(max_length=1500, null=False, blank=True)
    role_desc = models.CharField(max_length=1500, null=False, blank=True)

class UserRoles(BaseModel):
    user_id = models.ForeignKey(User, related_name='roles_user_id',null=False)
    role_id = models.ForeignKey(Roles, related_name='user_roles_id',null=False)

class Language(BaseModel):
    language_name = models.CharField(max_length=1500,null=True)
    user_id = models.ForeignKey(User, related_name='user_lang')

class UserProfile(BaseModel):
    user_id = models.ForeignKey(User, related_name='user_profile')
    name = models.CharField(max_length=1500, null=True, blank=True)
    phone_no = models.CharField(max_length=1500, null=False,unique=True, blank=False)
    otp_expiry = models.DateField(null=True, blank=True)
    otp = models.CharField(max_length=1500, null=False, blank=True)
    is_otp_verified = models.BooleanField(default=False)
    paytm_id =  models.CharField(max_length=1500, null=True, blank=True)
    is_paid = models.BooleanField(default=False)
    paytm_amount = models.DecimalField(max_digits=19, decimal_places=2,null=True)
    location_id = models.ForeignKey(Locations,related_name="user_location", null=True)
    profile_photo = models.CharField(max_length=1500, null=True, blank=True)
    lang_id=models.ForeignKey(Language,related_name='user_language', null=True) 
    block_reason = models.CharField(max_length=1500, null=True, blank=True)
    paytm_mobile_no = models.CharField(max_length=1500, null=True, blank=False)
    is_buyer = models.BooleanField(default=False)

class ResetPassword(BaseModel):
    user_id = models.ForeignKey(User,related_name="UserID")
    token = models.CharField(max_length=1500, null=True)


class Fields(BaseModel):
    field_name=models.CharField(max_length=1500,null=True)

class Language_fields(BaseModel):
    content=models.CharField(max_length=1500,null=True)
    field_id = models.ForeignKey(Fields,related_name='field_master')
    language_id=models.ForeignKey(Language,related_name='lang_master')

class Page(BaseModel):
    page_name= models.CharField(max_length=1500,null=True)
    

class Content(BaseModel):
    page_id=models.ForeignKey(Page,related_name='lang_page')
    content_text = models.CharField(max_length=2500,null=True)
    content_header = models.CharField(max_length=1500,null=True)
    language_id=models.ForeignKey(Language,related_name='langs_page')


class Farmer(BaseModel):
    user_id = models.ForeignKey(User,related_name='user_farmer')
    name =  models.CharField(max_length=1500,null=True)
    phone_no =  models.CharField(max_length=1500,null=False)
    land_area =  models.CharField(max_length=1500,null=True)
    otp =  models.CharField(max_length=1500,null=True)
    otp_expiry = models.DateField(null=True, blank=True)
    is_otp_verified = models.BooleanField(default=False)
    block_reason = models.CharField(max_length=1500, null=True, blank=True)
    address = models.CharField(max_length=1500,null=True)
    profile_photo = models.CharField(max_length=1500,null=True)
    
class LandRegistration(BaseModel):
    user_id = models.ForeignKey(User,related_name='land_farmer')
    land_name =  models.CharField(max_length=1500,null=True)
    land_address =  models.CharField(max_length=1500,null=True)
    farmer_id = models.ForeignKey(Farmer,related_name='farmer_land')
    land_acre =  models.CharField(max_length=1500,null=True)
    
class LandLocation(BaseModel):
    land_id=models.ForeignKey(LandRegistration,related_name='land_reg')
    lat = models.CharField(max_length=1500,null=True)
    lon =models.CharField(max_length=1500,null=True)
    order = models.CharField(max_length=1500,null=True)

class FarmerPastCrop(BaseModel):
    land_id=models.ForeignKey(LandRegistration,related_name='land_past_crop')
    past_crop = models.CharField(max_length=1500,null=True)
    past_crop_type = models.CharField(max_length=1500,null=True)
    past_yield =  models.DateField(null=True, blank=True)
    quantity_in_kg =   models.DecimalField(max_digits=19, decimal_places=2)
    past_price = models.DecimalField(max_digits=19, decimal_places=2)
    total_amount =  models.DecimalField(max_digits=19, decimal_places=2)

class Crop(BaseModel):
    crop_name = models.CharField(max_length=1500,null=True)
    crop_type = models.CharField(max_length=1500,null=True)
    tamil_crop_name = models.CharField(max_length=1500,null=True)
    crop_color = models.CharField(max_length=1500,null=True)
    is_organic = models.BooleanField(default=False)

class CropRegistration(BaseModel):
    master_crop_id = models.ForeignKey(Crop,related_name='crop')
    user_id = models.ForeignKey(User,related_name='land_farmer_det')
    land_id=models.ForeignKey(LandRegistration,related_name='land_reg_crop')
    planted_date =  models.DateField(null=True, blank=True)
    harvested_date =  models.DateField(null=True, blank=True)
    quantity_in_kg =   models.DecimalField(max_digits=19, decimal_places=2)
    unit = models.CharField(max_length=1500,null=True)
    expected_price = models.DecimalField(max_digits=19, decimal_places=2)
    total_amount =  models.DecimalField(max_digits=19, decimal_places=2)
    is_organic = models.BooleanField(default=False)
    crop_desc = models.CharField(max_length=1500,null=True)

class VerifiedCrop(BaseModel):
    crop_id = models.ForeignKey(CropRegistration,related_name='crop')
    success_count =  models.BigIntegerField(default=0,null=True,blank=True)
    failure_count =  models.BigIntegerField(default=0,null=True,blank=True)
    is_verified = models.BooleanField(default=False)

class TempCropImage(BaseModel):
    crop_id = models.ForeignKey(CropRegistration,related_name='temp_crop')
    crop_lat = models.CharField(max_length=1500,null=True)
    crop_long = models.CharField(max_length=1500,null=True)
    img_path =  models.ImageField(upload_to='crop_img/',null=True, blank=True)
    verified_crop_id = models.ForeignKey(VerifiedCrop,related_name='crop_update')

class TempVerify(BaseModel):
    temp_crop_id = models.ForeignKey(TempCropImage,related_name='verify_crop')
    user_id = models.ForeignKey(User,related_name='verify_user')

class UserContact(BaseModel):
    user_id = models.ForeignKey(User,related_name='contact_user')
    farmer_id = models.ForeignKey(Farmer,related_name='contacted_farmer')
    contact_type = models.CharField(max_length=1500,null=True)
    
