from django.apps import AppConfig


class AgriapiConfig(AppConfig):
    name = 'AgriApi'
