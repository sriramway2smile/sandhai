from rest_framework.response import Response
from django.http import HttpResponse
from django.db.models import Q
from .models import *
from .serializer import *
from . import json
from . import helpers
from rest_framework.decorators import api_view, permission_classes
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from django.db import transaction
from rest_framework import generics
from rest_framework_jwt.settings import api_settings
from rest_framework.permissions import AllowAny
from django.template.loader import get_template
from django.template import Context
import pdfkit
from django.shortcuts import render
from datetime import datetime
from django.core.mail import send_mail
import smtplib
from email.mime.text import MIMEText
from django.core.exceptions import ObjectDoesNotExist

from rest_framework.exceptions import APIException
from django.utils.crypto import get_random_string
from datetime import datetime, timedelta
from decimal import *

from django.core.paginator import Paginator
from json import dumps, loads

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

#Manage Admin Profile
@api_view(['GET'])
def user_profile_info(request):
    serializer = UserSerializer(request.user)
    return Response(serializer.data)

#update  users paytm acc
@api_view(['POST'])
@transaction.atomic
def update_paytm(request):
    # check whether the method is post or get
    if request.method == 'POST':
        #check whether user is authenticated
        auth = AuthToken.objects.get(apiKey=helpers.header(request))
        # get user data
        user = UserProfile.objects.get(user_id=auth.user_id)
        if user:
            # if user is not null then request is convert into json
            data = json.ParseData(request)
            if data:
                if(data["paytm_no"]):
                    #update paytm data in user
                    user.paytm_id = data["paytm_no"]
                    user.save()

        else:
            json.Response("Token InValid",False)


    return json.Response("PayTm No Updated")


# Admin profile edit
@csrf_exempt
def update_profile(request):
    if request.method == 'POST':
        data = json.ParseData(request)
        userEmail = data['email']
        userFirstName = data['name']
        if userFirstName  and userEmail:
            print("helo1")
            user = User.objects.get(id=data['id'])
            if user:
                print("helo2")
                try:
                    user = User.objects.get(email=data['email']).exclude(id=data['id'])
                    return json.Response('User Already Exits')
                except:
                    user.email = userEmail
                    user.username = userEmail
                    user.first_name = userFirstName
                    user.save()

                    user_profile = UserProfile.objects.get(user_id=user.id)
                    if user_profile:
                        user_profile.phone_no=data['phone_no']
                        user_profile.profile_photo=data['profile_photo']
                        user_profile.save()

            else:
                return json.Response('User Already Exits')
        else:
            return json.Response('Check Inputs')
        return json.Response('Profile Updated')

# admin password update
@csrf_exempt
@transaction.atomic
def update_password_profile(request):
    if request.method == 'POST':
        import json as j
        data =  j.loads(request.body.decode('utf-8'))
        # data = json.ParseData(request)
        userEmail = data['email']
        userCurrentPassword = data['old_password']
        userNewPassword = data['password']
        if userCurrentPassword and userNewPassword and userEmail:
            user = User.objects.get(email=userEmail)
            if user:
                if user.check_password(userCurrentPassword):
                    user.email = userEmail
                    user.set_password(userNewPassword)
                    user.save()
                else:
                    return json.Response('Current Password not match', False)
            else:
                return json.Response('User Email Already Exits', False)
        else:
            return json.Response('Check Inputs', False)
        return json.Response('Password Updated')


# @csrf_exempt
# @transaction.atomic
# def admin_update_password_profile(request):
#     if request.method == 'POST':
#         data = json.ParseData(request)
#         userEmail = data['email']
#         userCurrentPassword = data['old_password']
#         userNewPassword = data['new_password']
#         if userCurrentPassword and userNewPassword and userEmail:
#             user = User.objects.get(email=userEmail)
#             if user:
#                 user.set_password(userNewPassword)
#                 user.save()
#             else:
#                 return json.Response('User Email Already Exits ?', False)
#         else:
#             return json.Response('Check Inputs', False)
#         return json.Response('Password Updated')


# @csrf_exempt
# @transaction.atomic
# def sample(request):
#     requestData = json.ParseData(request)
#     serializer = LocationSerializer(data={
#         'building_no': requestData.get('building_no', ''),
#         'created_by': ''
#     })
#     if serializer.is_valid():
#         location = serializer.save()
#     else:
#         print(serializer.errors)
#     return


# For Email Function
def sendCustomMail(message,recepiants,subject):
    me = "dinesh.w2sdev@gmail.com"
    you = [recepiants]
    password = "dasskali14"
    msg = MIMEText(message,'html')
    msg['Subject'] = subject
    msg['From'] = me
    msg['To'] = ", ".join(you)
    session = smtplib.SMTP("smtp.gmail.com:587")
    session.ehlo()
    session.starttls()
    session.login(me, password)
    session.sendmail(me, you, msg.as_string())
    session.quit()

#Admin Login
@csrf_exempt
@transaction.atomic
def login(request):
   import json as j
   requestData =  j.loads(request.body.decode('utf-8'))
   # requestData = json.ParseData(request)
   print(requestData)
   #check whether user is authenticated
   user = helpers.auth_user(requestData.get(
       'email', ''), requestData.get('password', ''))

   if user['status']:
       return json.Response('Unable to log in with provided credentials.', False)
   if user['user']:
        # generate token for authenticated user
       auth_token = helpers.auth_token(user['user'], request)
       if auth_token:
        # return generated token for the user
           return json.Response(auth_token)
       else:
           transaction.set_rollback(True)
           return json.Response('Check Headers', False)
   else:
       return json.Response('User name or Password Wrong', False)
   return json.Response('')


#SMS_Integration
def sms_push(to,message):
    import http.client
    import json as j
    conn = http.client.HTTPConnection("api.msg91.com")
    ph_no ='+91'+to
    url = "http://api.msg91.com/api/sendhttp.php?sender=SANDAI&route=4&mobiles="+ph_no+"&authkey=229426AytWo7gVaUl5b628c40&country=91&message="+message+"&response=json"
    conn.request("GET", url)
    res = conn.getresponse()
    data = res.read()
    print("helo")
    data = j.loads(data.decode("utf-8"))
    print(data['type'])
    if(data['type']=='success'):
        return True
    else:
        return False
    # import json as j
    # import urllib.request
    # ph_no ='+91'+to
    

    # authkey = "YourAuthKey" # Your authentication key.

    # mobiles = "9999999999" # Multiple mobiles numbers separated by comma.

    # message = "Test message" # Your message to send.

    # sender = "112233" # Sender ID,While using route4 sender id should be 6 characters long.

    # route = "default" # Define route

    # # Prepare you post parameters
    # values = {
    #           'authkey' : authkey,
    #           'mobiles' : mobiles,
    #           'message' : message,
    #           'sender' : sender,
    #           'route' : route
    #           }


    # url = "http://api.msg91.com/api/sendhttp.php?sender=MSGIND&route=4&mobiles="+ph_no+"&authkey=229426AytWo7gVaUl5b628c40-12443242&country=0&message="+message+"&response=json"
    # request = urllib.request.Request(url)
    # response = urllib.request.urlopen(request)
    # print (response.read().decode('utf-8'))
    # import http.client

    # conn = http.client.HTTPConnection("api.msg91.com")

    # payload = { "sender": "SOCKET", "route": "4", "country": "91", "sms": [ { "message": "Message1", "to": [ "9003156866"] }] }

    # headers = {
    #     'authkey': "",
    #     'content-type': "application/json"
    #     }

    # conn.request("POST", "/api/v2/sendsms", payload, headers)

    # res = conn.getresponse()
    # data = res.read()

    # print(data.decode("utf-8"))

    # to=to
    # message=message
    # apiKey = "-GCQ-zYeTweFiMWEonZcHw=="
    # headers = {'Authorization': apiKey , 'Content-Type': 'application/json', 'Accept': 'application/json'}
    # data = {}
    # data = {'to': ['+91'+to], 'content': message}
    # http = httplib2.Http()
    # body = urllib3.request.urlencode(data)
    # url = "https://platform.clickatell.com/messages"
    # method = "POST"
    # resp , response = http.request(url, method, headers=headers, body=json.dumps(data))
    # requestData = j.loads(response.decode('utf-8'))
    # print(requestData["messages"])
    # print(len(requestData["messages"]))
    # if(len(requestData["messages"])==0):
    #     return False
    # else:
    #     return True
    

#Surveyor Login
@csrf_exempt
@transaction.atomic
def check_user(request):
    import json as j
    import random
    requestData = j.loads(request.body.decode('utf-8'))
    #print(requestData)
    #check Authenticated Surveyor
    phone_no = requestData.get('phone_no', '')
    #user = UserProfile.objects.get(phone_no=phone_no)
    #print(phone_no)
    # otp = '1234';
    #user = UserProfile.objects.get(phone_no=phone_no).update(otp=otp)

    #Enable otp value in prod
    otp = random.randint(1000,9999)
    # otp = 1234
    #return json.Response(requestData.get('is_farmer', ''))
    if(requestData.get('is_farmer', '')):
        try:
            farmer = Farmer.objects.get(phone_no=requestData.get('phone_no', ''))
            msg = "Your Sandhai OTP is {otp}".format(otp=otp)
            err = "Something went wrong in OTP sending"
            suc = "OTP has been send successfully"
            #print(msg)
            check = sms_push(phone_no,msg)
            print(check)
            farmer.otp = otp
            farmer.save();
            otp = farmer.otp
            return json.Response({'otp': otp,})

        except:
            # otp = "1234"
            return json.Response("Number Not Registered",False)
    try:
        user = UserProfile.objects.get(phone_no=phone_no)
        #print(user)
        
        msg = "Your Sandhai OTP is {otp}".format(otp=otp)
        err = "Something went wrong in OTP sending"
        suc = "OTP has been send successfully"
        #print(msg)
        check = sms_push(phone_no,msg)
        print(check)
        
        # return otp_resp
        if(check==True):
            try:
                user.otp = otp
                user.name = requestData.get('name', '')
                user.save();
                return json.Response({'send_success': suc}) 
            except:
                return json.Response({'send_error': err})
                
    except:
        datas =  j.loads(request.body.decode('utf-8'))
        requestData = j.loads(request.body.decode('utf-8'))
        #print(requestData)
        otp = random.randint(1000,9999)
        # otp = 1234
        msg = "Your Sandhai OTP is {otp}".format(otp=otp)
        err = "Something went wrong in OTP sending"
        suc = "OTP has been send successfully"
        #print(msg)
        check = sms_push(phone_no,msg)
        data = {}
        context = {
            "request": request.body,
        }
        datas['first_name'] = ""
        data['username'] = get_random_string(length=32)
        data['first_name'] = datas['first_name']
        if(datas['phone_no']):
            try:
                serializer = UserSerializer(data=data, context=context)
                if serializer.is_valid():
                    data = serializer.save()
                    data.set_password(data.password)
                    data.save()
                    user = data
                    otp=otp
                    serializer = UserProfileSerializer(data={
                        'phone_no': datas['phone_no'] or '',
                        'location_id':1,
                        'user_id':user.id,
                        'name':datas['name'] or '',
                        'otp':otp,
                    }, context=context)
                    if serializer.is_valid():
                        Profile = serializer.save()
                    else:
                        return json.Response(serializer.errors, False)

                    role = Roles.objects.get(role_name='Creator')
                    serializer = UserRoleSerializer(data={
                        'user_id': user.id,
                        'role_id': role.id
                    }, context=context)
                    if serializer.is_valid():
                        role = serializer.save()
                    else:
                        return json.Response(serializer.errors, False)
                else:
                    return json.Response(serializer.errors, False)
            except:
                return json.Response("Number Not Registered",False)

            return json.Response({'success': "otp sent successfully",})
        else:
            return json.Response("Data Cannot be empty", False)




#Surveyor Login
@csrf_exempt
@transaction.atomic
def user_login(request):
    import json as j
    requestData = j.loads(request.body.decode('utf-8'))
    print(requestData)

    if (requestData.get('is_farmer','')):
        try:
            Farmer.objects.get(phone_no = requestData.get('phone_no', ''),otp = requestData.get('otp', ''))
            if Farmer:
                return json.Response("Success")
            else:
                return json.Response("OTP InValid",False)
        except:
            return json.Response("OTP InValid",False)
    #check Authenticated Surveyor
    user = helpers.auth_surveyor(requestData.get(
    'phone_no', ''), requestData.get('otp', ''))
    if user['status']:
        return json.Response('Unable to log in with provided credentials.', False)
    if user['user']:
        try:
            user = User.objects.get(username=user['user'],is_active=True)
       
            auth_token = helpers.auth_token(user, request)
            print(auth_token)
            user_profile = UserProfile.objects.filter(user_id=user.id)
            user_profile = user_profile[0]
            if(user_profile.paytm_id=='' or user_profile.paytm_id==None):
                paytm = 0;
            else:
                paytm = 1;

            if auth_token:
                data = {"token":auth_token,"is_paytm_acc":paytm}
                return json.Response(data)
            else:
                transaction.set_rollback(True)
                return json.Response('Check Headers', False)
        except:
            return json.Response('Your account has been blocked,please contact Admin', False)
    else:
        return json.Response('User name or Password Wrong', False)
    return json.Response('')

#set roles for the user
@csrf_exempt
@api_view(['GET'])
@permission_classes((AllowAny,))
def create_roles(request):
    Roles.objects.all().delete()
    roles = [
        {
            'role_name': 'SuperAdmin',
            'role_desc': 'Admin User'
        },
        {
            'role_name': 'Creator',
            'role_desc': 'Surveyor'
        },
        {
            'role_name': 'User',
            'role_desc': 'Farmer'
        }
    ]
    temp = 1
    role_id = 1
    for role in roles:
        serializer = RolesSerializer(data=role)
        if serializer.is_valid():
            data = serializer.save()
            if temp == 1:
                temp = 2
                role_id = data.id
    user = User.objects.get(pk=1)
    UserRoles.objects.all().delete()
    roles = [
        {
            'user_id': user.pk,
            'role_id': role_id,
            'added_by': ''
        }
    ]
    temp = 1
    for role in roles:
        serializer = UserRoleSerializer(data=role)
        if serializer.is_valid():
            data = serializer.save()
        else:
            pass
    loc = Locations(
        address= 'Chennai');
    loc.save()
    serializer = UserDevProerializer(data={
        'user_id': user.id,
        'gender': "MALE",
        'phone_no': "phone_no",
        'dob': "1996-05-22",
        'age': '21',
        'location_id': loc.id,
    })
    if serializer.is_valid():
        data = serializer.save()
    else:
        pass

    return json.Response('ok')

# #Reset Paytm
# @csrf_exempt
# @transaction.atomic
# def paytm(self,request):
#     import json as j
#     if request.method == 'POST':
#         data = j.loads(request.body.decode('utf-8'))
#         user = self.request.user
#         paytmId = data['paytmId']
#         phone_no = data['phone_no']
#         if(paytmId and phone_no):
#             user = Userprofile.objects.get(user_id=user.id)
#             user.paytm_id=paytmId
#             user.paytm_mobile_no = phone_no
#             user.save()
#             return json.Response("success")
#         else:
#             return json.Response("Failed",False)


#Register Surveyor
@csrf_exempt
@transaction.atomic
def register(request):
    import json as j
    import random
    if request.method == 'POST':
        datas =  j.loads(request.body.decode('utf-8'))
        requestData = j.loads(request.body.decode('utf-8'))
        print(requestData)
        data = {}
        context = {
            "request": request.body,
        }
        data['username'] = get_random_string(length=32)
        data['first_name'] = datas['first_name']
        if(datas['first_name'] and datas['phone_no']):
            serializer = UserSerializer(data=data, context=context)
            if serializer.is_valid():
                data = serializer.save()
                data.set_password(data.password)
                data.save()
                user = data
                otp = random.randint(1000,9999)
                # otp = 1234
                serializer = UserProfileSerializer(data={
                    'phone_no': datas['phone_no'] or '',
                    'location_id':1,
                    'user_id':user.id,
                    'otp':otp,
                }, context=context)
                number = datas['phone_no']
                msg = "Your Sandhai OTP is {otp}".format(otp=otp)
                err = "Something went wrong in OTP sending"
                suc = "OTP has been send successfully"
                #print(msg)
                check = sms_push(number,msg)
                print(check)
                if serializer.is_valid():
                    if (check==True):
                        Profile = serializer.save()
                        return json.Response({ 'send_success':suc}) 
                    else:
                        return json.Response({'send_error': err}) 
                else:
                    return json.Response(serializer.errors, False)

                role = Roles.objects.get(role_name='Creator')
                serializer = UserRoleSerializer(data={
                    'user_id': user.id,
                    'role_id': role.id
                }, context=context)
                if serializer.is_valid():
                    role = serializer.save()
                else:
                    return json.Response(serializer.errors, False)
            else:
                return json.Response(serializer.errors, False)
            return json.Response({'data': 'Added'})
        else:
            return json.Response("Data Cannot be empty", False)

#Admin Reset Password URL Setting
@csrf_exempt
@transaction.atomic
def forgotpassword(request):
    import json as j
    if request.method == 'POST':

        data = j.loads(request.body.decode('utf-8'))
        user_id = data['username']
        # try:
        user = User.objects.get(email=user_id)
        token = jwt_encode_handler(jwt_payload_handler(user))
        reset = ResetPassword(user_id=user,token=token)
        reset.save()
        subject = "Reset Password"
        recepiants = user_id
        content = "Please click the url to reset the password. http://13.127.62.77/reset/"+token

        sendCustomMail(content,recepiants,subject)
        return json.Response(token)

#set username when reset url hit
@csrf_exempt
@transaction.atomic
def updatepassword(request):
    import json as j
    if request.method == 'POST':

        data = j.loads(request.body.decode('utf-8'))
        # print(data)
        token = data['id']
        # print(token)
        # try:
        user = ResetPassword.objects.filter(token=token).latest('pk')
        user = django.core.serializers.serialize('json',[user])
        user = j.loads(user)
        user = user[0]['fields']
        print(user['user_id'])
        user_data = User.objects.get(id=user['user_id'])
        user = django.core.serializers.serialize('json',[user_data])
        user_data = j.loads(user)
        user_data = user_data[0]['fields']['username']
        return json.Response(user_data)

#Reset Password when forgot
@csrf_exempt
@transaction.atomic
def update_password(request):
    import json as j
    if request.method == 'POST':
        data = j.loads(request.body.decode('utf-8'))

        email = data['email']
        password = data['password']
        if(email and password):
            user = User.objects.get(email=email)
            user.set_password(password)
            user.save()
            return json.Response("success")
        else:
            return json.Response("Failed",False)

#Display Surveyor list
class Users(generics.ListAPIView):
    serializer_class = UserSerializer
    def get_queryset(self):
        user_role_id = 2
        if(self.request.query_params.get('role')):
            user_role_id = self.request.query_params.get('role')
        UserData = UserRoles.objects.filter(
            role_id=user_role_id).values('user_id').distinct()
        queryset = User.objects.filter(id__in=UserData).order_by('-pk')
        return queryset

class buyer(generics.ListAPIView):
    serializer_class = UserSerializer
    def get_queryset(self):
        user_role_id = 2
        if(self.request.query_params.get('role')):
            user_role_id = self.request.query_params.get('role')
        UserData = UserRoles.objects.filter(
            role_id=user_role_id).values('user_id').distinct()
        Userprofile = UserProfile.objects.filter(is_buyer=True).values('user_id').distinct()
        queryset = User.objects.filter(id__in=UserData).filter(id__in=Userprofile).order_by('-pk')
        return queryset


#display language list
class language_master(generics.ListCreateAPIView):
    serializer_class = LanguageSerializer
    queryset = Language.objects.all()

#display page list
class page_master(generics.ListCreateAPIView):
    serializer_class = PageSerializer
    queryset = Page.objects.all()

#Page Content Add
class contentupdate(generics.ListCreateAPIView):
    serializer_class = ContentSerializer
    def get_queryset(self):
        return Content.objects.filter(deleted_at__isnull=True)

    def create(self, serializer):
        user = self.request.user
        data=self.request.data
        print(data)
        lan = Language.objects.get(pk=data['languages'])
        page = Page.objects.get(pk=data['page_name'])

        content = Content(
            page_id= page,
            content_text = data['content'],
            language_id =lan,
            )
        content.save()

        return json.Response("success")

#Page Content Update
class page_content_update(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = ContentSerializer
    def get_queryset(self):
        return Content.objects.filter(deleted_at__isnull=True)

    def perform_update(self, serializer):
        user = self.request.user
        data=self.request.data

        lan = Language.objects.get(pk=data['languages'])
        page = Page.objects.get(pk = data['page_name'])

        content = Content.objects.get(id=data['id'])
        content.page_id= page
        content.content_text = data['content']
        content.language_id =lan
        content.save()

        return json.Response("success")

#Delete Page Content
@csrf_exempt
@transaction.atomic
def page_content_delete(request, pk):

    content = Content.objects.get(id=pk)
    content.deleted_at =datetime.now()
    content.save()

    return json.Response("success")

#Block Surveyor/farmer from Admin
@csrf_exempt
@transaction.atomic
def block_user(request, pk):
    import json as j
    data = j.loads(request.body.decode('utf-8'))
    print(data)
    if(data['val']==1):
        farmer = Farmer.objects.get(pk=pk)
        if(farmer.is_active==True):
            farmer.is_active = False
            farmer.block_reason = data['result']
        else:
            farmer.block_reason = data['result']
            farmer.is_active = True
        farmer.save()

    else:
        user = User.objects.get(pk=pk)
        user_profile = UserProfile.objects.get(user_id=user)
        if(user.is_active == True):
            user.is_active = False
            user_profile.block_reason = data['result']
        else:
            user.is_active = True
            user_profile.block_reason = data['result']
        user.save()
        user_profile.save()
    return json.Response("success")

#reset OTP for Both user,farmer
@csrf_exempt
@transaction.atomic
def reset_otp(request, pk):

    import json as j
    data = j.loads(request.body.decode('utf-8'))

    print(data)

    if(data['status']==1):
        user = Farmer.objects.get(pk=pk)
        if(user.is_active == True):
            farmer = Farmer.objects.get(pk=pk)
            farmer.otp = data['value']
            farmer.save()
            return json.Response("success")
        else:
            return json.Response("Deactivated User",False)
    else:
        user = User.objects.get(pk=pk)
        if(user.is_active == True):
            userprofile = UserProfile.objects.get(user_id=user)
            userprofile.otp = data['value']
            userprofile.save()
            return json.Response("success")
        else:
            return json.Response("Deactivated User",False)

#Register Farmer Details
class add_farmer(generics.ListCreateAPIView):
    serializer_class = FarmerSerializer
    def get_queryset(self):
        user = self.request.user
        if user.id==1:
            # for admin
            return Farmer.objects.filter(deleted_at__isnull=True).order_by('-pk')

        else:
            # for user
            return Farmer.objects.filter(deleted_at__isnull=True,created_by=user).order_by('-pk')

    def create(self, serializer):
        import random
        user = self.request.user
        data=self.request.data
        print(data)
        if(data['phone_no'] and data['name'] and data['address'] ):
            # mbl_no = UserProfile.objects.filter(phone_no=data['phone_no']).count();
            # print(mbl_no)
            # if(mbl_no>=1):
            #     return json.Response("Phone No already exists",False)

            user_id = User.objects.get(username=user)
            user_no = Farmer.objects.filter(phone_no=data['phone_no']).count();
            if(user_no>=1):
                return json.Response("Phone No already exists",False)
            # otp = random.randint(1000,9999)
            otp=1234
            phone_no = data['phone_no']
            u_msg = "Your Sandhai OTP is {otp}".format(otp=otp)
            u_err = "Something went wrong in OTP sending"
            u_suc = "OTP has been send successfully"
            u_check = sms_push(phone_no,u_msg)
            if(u_check==True):
                try:
                    farmer = Farmer(
                            name =data.get('name',''),
                            phone_no= data.get('phone_no',''),
                            address = data.get('address',''),
                            created_by=user_id,
                            user_id = user_id,
                            profile_photo = data.get('profile', ''),
                            otp=otp
                            )
                    farmer.save()
                    return json.Response(farmer.id,True)
                except:
                    return json.Response({'send_error':u_err})
            # farmer = Farmer(
            #         name =data.get('name',''),
            #         phone_no= data.get('phone_no',''),
            #         address = data.get('address',''),
            #         created_by=user_id,
            #         user_id = user_id,
            #         profile_photo = data.get('profile', ''),
            #         otp='1234'
            #         )
            # farmer.save()

            # return json.Response(farmer.id,True)
        else:
            return json.Response("Data Cannot be empty",False)

#Register Land Details of Farmer
class land_registration(generics.ListCreateAPIView):
    serializer_class = LandLocationSerializer
    def get_queryset(self):
        user = self.request.user
        if user.id==1: # for admin
            try:
                page = self.request.GET.get('page', '')
                land = LandRegistration.objects.filter(deleted_at__isnull=True).order_by('-pk')
                land = Paginator(land, 10)
                queryset = land.page(page)
                return queryset
            except:
                return LandRegistration.objects.filter(deleted_at__isnull=True).order_by('-pk')

        else: # for user
            return LandRegistration.objects.filter(deleted_at__isnull=True,created_by=user).order_by('-pk')

    def create(self, serializer):
        from shapely.geometry import Point, Polygon,LinearRing
        from shapely.geometry.polygon import Polygon
        import numpy as np
        from shapely import geometry

        user = self.request.user
        datas=self.request.data
        # print(data)
        offline = isinstance(datas, list)
        # print(user)
        user_phone = UserProfile.objects.get(user_id=user)
        mob_number = user_phone.phone_no
        not_allowed = False;
        if(offline):
            
            for data in datas:
                lat_lon = []
                check_land_reg = LandRegistration.objects.all()
                if(len(check_land_reg)>0):
                    # data['land_loc'][0]['order']=len(data['land_loc'])+1
                    first_point = data['land_loc'][0]
                    first_point['order'] = len(data['land_loc'])+1
                    data['land_loc'].append(first_point)
                    
                    for land_loc_val in check_land_reg:
                        print(data['land_loc'][0])
                        land_loc = LandLocation.objects.filter(land_id=land_loc_val.id)
                        lat=[]
                        lon=[]
                        poly1 = geometry.Polygon([[float(p.lat), float(p.lon)] for p in land_loc])
                        print(poly1.wkt)
                        poly2 = geometry.Polygon([[float(s['lat']), float(s['lon'])] for s in data['land_loc']])
                        print(poly2.wkt)
                        # if(poly1.intersects(poly2) and not poly1.crosses(poly2) and not poly1.contains(poly2) ):
                        #     print("allowed")
                        # else:
                        #     print("not allowed")
                        if ((poly1.intersects(poly2) == False) and (poly1.disjoint(poly2) == True)) or ((poly1.intersects(poly2) == True) and (poly1.touches(poly2) == True)):
                            allowed = True

                            print("Allowed")
                        
                        else:
                            land_name=data['land_name']
                            not_allowed = True
                            print("not")
                            print(land_name)
                            farmer = Farmer.objects.get(pk=data['farmer_id'])
                            farmer_name = farmer.name
                            farmer_number = farmer.phone_no
                            msg = "Sandhai - We have removed your duplicate land"
                            err = "Something went wrong message sending"
                            check = sms_push(farmer_number,msg)
                            if (check==False):
                                pass;
                                #return json.Response({"send_error" : err})
                            return json.Response("Land Already Mapped",False)

                        if ((poly2.intersects(poly1) == False) and (poly2.disjoint(poly1) == True)) or ((poly2.intersects(poly1) == True) and (poly2.touches(poly1) == True)):
                            allowed = True

                            print("Allowed")
                        
                        else:
                            land_name=data['land_name']
                            not_allowed = True
                            print("not")
                            farmer = Farmer.objects.get(pk=data['farmer_id'])
                            farmer_name = farmer.name
                            farmer_number = farmer.phone_no
                            msg = "Sandhai - We have removed your duplicate land"
                            err = "Something went wrong message sending"
                            check = sms_push(farmer_number,msg)
                            if (check==False):
                                pass;
                                #return json.Response({"send_error" : err})
                            return json.Response("Land Already Mapped",False)



                    if(not not_allowed):
                        farmer = Farmer.objects.get(pk = data['farmer_id'])
                        land = LandRegistration(
                                farmer_id =farmer,
                                land_name= data['land_name'],
                                land_address= data['land_address'],
                                land_acre=data['acre'],
                                created_by=user,
                                user_id = user
                                )
                        land.save()

                        for land_loc in data['land_loc'] :
                            
                            land_loc = LandLocation(
                            land_id = land,
                            lat = land_loc['lat'],
                            lon = land_loc['lon'],
                            order = land_loc['order'],
                            )
                            land_loc.save()
                        # elif (poly1.intersects(polMe) == True) and (poly1.disjoint(polMe) == False) and (poly1.touches(polMe) == False):
                        #     allowed = False
                        # for lat_lng in land_loc:
                        #     lat.append(lat_lng.lat)
                        #     lon.append(lat_lng.lon)
                        # lats_vect = np.array(lat)
                        # lons_vect = np.array(lon)
                    
                        # lons_lats_vect = np.column_stack((lats_vect,lons_vect)) # Reshape coordinates
                        # print(lons_lats_vect)
                        # if(len(poly.wkt)>3):

                        #     polygon = poly.wkt # create polygon
                        #     print(polygon)
                        #     lat1=[]
                        #     lon1=[]
                        #     for lat_loc_val in data['land_loc'] :
                        #         lat1.append(lat_loc_val['lat'])
                        #         lon1.append(lat_loc_val['lon'])
                        #     lats_vect1 = np.array(lat1)
                        #     lons_vect1 = np.array(lon1)
                        
                        #     new_lons_lats_vect = np.column_stack((lats_vect1,lons_vect1)) # Reshape coordinates
                        #     # print(new_lons_lats_vect)
                        #     if(len(new_lons_lats_vect)>3):
                        #         # pt= Polygon(new_lons_lats_vect)
                                
                        #         pt = Point(float(lat_loc_val['lat']),float(lat_loc_val['lon']))
                        #     # print(pt)
                        #     # linearring = LinearRing(list(poly.exterior.coords))
                        #     # print(poly.touches(linearring))
                        #     # print(linearring.contains(pt))
                        #     print(poly.contains(pt))
                        #     # print(polygon.intersects(pt))
                        #     # print(polygon.disjoint(pt))
                        #     # print(polygon.touches(pt))
                            
                        #     print("already mapped");
                        #     if((poly.touches(linearring) == True) and (poly.disjoint(pt) == False)):
                        #     # if((polygon.intersects(pt) == True) or (polygon.disjoint(pt) == False)):
                        #         # return json.Response("Already Mapped Land",False)
                        #         print("already mapped");
                        #     else:
                        #     # return json.Response("Already Mapped Land",False)
                        #         print("hello")
                        #         farmer = Farmer.objects.get(pk = data['farmer_id'])
                        #         land = LandRegistration(
                        #                 farmer_id =farmer,
                        #                 land_name= data['land_name'],
                        #                 land_address= data['land_address'],
                        #                 land_acre=data['acre'],
                        #                 created_by=user,
                        #                 user_id = user
                        #                 )
                        #         land.save()

                        #         for land_loc in data['land_loc'] :
                                    
                        #             land_loc = LandLocation(
                        #             land_id = land,
                        #             lat = land_loc['lat'],
                        #             lon = land_loc['lon'],
                        #             order = land_loc['order'],
                        #             )
                        #             land_loc.save()
                else:
                    
                    farmer = Farmer.objects.get(pk = data['farmer_id'])
                    land = LandRegistration(
                            farmer_id =farmer,
                            land_name= data['land_name'],
                            land_address= data['land_address'],
                            land_acre=data['acre'],
                            created_by=user,
                            user_id = user
                            )
                    land.save()

                    for land_loc in data['land_loc'] :
                        
                        land_loc = LandLocation(
                        land_id = land,
                        lat = land_loc['lat'],
                        lon = land_loc['lon'],
                        order = land_loc['order'],
                        )
                        land_loc.save()
                        
            return json.Response({"id":data["id"],"land_id":land.id})
            # return json.Response("success")
            
        else:
            if(datas):
                lat_lon = []
                check_land_reg = LandRegistration.objects.all()
                if(len(check_land_reg)>0):
                    # to Make first and last point same
                    # datas['land_loc'][0]['order']=len(datas['land_loc'])+1

                    first_point = datas['land_loc'][0]
                    first_point['order'] = len(datas['land_loc'])+1
                    datas['land_loc'].append(first_point)
                    
                    for land_loc_val in check_land_reg:
                        land_loc = LandLocation.objects.filter(land_id=land_loc_val.id)
                        lat=[]
                        lon=[]
                        poly1 = geometry.Polygon([[float(p.lat), float(p.lon)] for p in land_loc])
                        print(poly1.wkt)
                        poly2 = geometry.Polygon([[float(s['lat']), float(s['lon'])] for s in datas['land_loc']])
                        print(poly2.wkt)
                        # if(poly1.intersects(poly2) and not poly1.crosses(poly2) and not poly1.contains(poly2) ):
                        #     print("allowed")
                        # else:
                        #     print("not allowed")
                        if ((poly1.intersects(poly2) == False) and (poly1.disjoint(poly2) == True)) or ((poly1.intersects(poly2) == True) and (poly1.touches(poly2) == True)):
                            allowed = True

                            print("Allowed")
                            
                        else:
                            not_allowed = True
                            print("not")

                        if ((poly2.intersects(poly1) == False) and (poly2.disjoint(poly1) == True)) or ((poly2.intersects(poly1) == True) and (poly2.touches(poly1) == True)):
                            allowed = True

                            print("Allowed")
                        
                        else:
                            land_name=datas['land_name']
                            not_allowed = True
                            print("not")
                            farmer = Farmer.objects.get(pk=datas['farmer_id'])
                            farmer_name = farmer.name
                            farmer_number = farmer.phone_no
                            msg = "Sandhai - We have removed your duplicate land"
                            err = "Something went wrong message sending"
                            check = sms_push(farmer_number,msg)
                            if (check==False):
                                pass;
                                #return json.Response({"send_error" : err})
                            return json.Response("Land Already Mapped",False)

                        
                    if(not not_allowed):
                    # for lat_lng in land_loc:
                    #     lat.append(lat_lng.lat)
                    #     lon.append(lat_lng.lon)
                    # lats_vect = np.array(lat)
                    # lons_vect = np.array(lon)
                
                    # lons_lats_vect = np.column_stack((lats_vect,lons_vect)) # Reshape coordinates
                    # if(len(lons_lats_vect)>3):
                    #     polygon = Polygon(lons_lats_vect) # create polygon
                    #     for lat_loc_val in data['land_loc'] :
                    #         pt = Point(float(lat_loc_val['lat']),float(lat_loc_val['lon']))
                    #         if(polygon.intersects(pt)):
                    #             return json.Response("Already Mapped Land",False)

                        farmer = Farmer.objects.get(pk = datas['farmer_id'])
                        land = LandRegistration(
                                farmer_id =farmer,
                                land_name= datas['land_name'],
                                land_address= datas['land_address'],
                                land_acre=datas['acre'],
                                created_by=user,
                                user_id = user
                                )
                        land.save()

                        for land_loc in datas['land_loc'] :
                            
                            land_loc = LandLocation(
                            land_id = land,
                            lat = land_loc['lat'],
                            lon = land_loc['lon'],
                            order = land_loc['order'],
                            )
                            land_loc.save()
                        
                        return json.Response({"id":datas["id"],"land_id":land.id})
                    else:
                        farmer = Farmer.objects.get(pk=datas['farmer_id'])
                        farmer_name = farmer.name
                        farmer_number = farmer.phone_no
                        msg = "Sandhai - We have removed your duplicate land"
                        err = "Something went wrong message sending"
                        check = sms_push(farmer_number,msg)
                        if (check==False):
                            pass;
                            #return json.Response({"send_error" : err})
                        return json.Response("Land Already Mapped",False)
                else:
                    farmer = Farmer.objects.get(pk = datas['farmer_id'])
                    land = LandRegistration(
                            farmer_id =farmer,
                            land_name= datas['land_name'],
                            land_address= datas['land_address'],
                            land_acre=datas['acre'],
                            created_by=user,
                            user_id = user
                            )
                    land.save()

                    for land_loc in datas['land_loc'] :
                        
                        land_loc = LandLocation(
                        land_id = land,
                        lat = land_loc['lat'],
                        lon = land_loc['lon'],
                        order = land_loc['order'],
                        )
                        land_loc.save()
                    print(datas)
                    return json.Response({"id":datas["id"],"land_id":land.id})
                    # return json.Response("success")

            else:
                return json.Response("Data Cannot be empty",False)

# Get Land View of Farmer
class edit_land_details(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = LandLocationSerializer
    def get_queryset(self):
        return LandRegistration.objects.filter(deleted_at__isnull=True)

    def perform_update(self, serializer):
        user = self.request.user
        data=self.request.data

        return json.Response("success")

#Register Crop in land
class add_crop(generics.ListCreateAPIView):
    
    serializer_class = CropregSerializer
    def get_queryset(self):
        queryset = CropRegistration.objects.filter(deleted_at__isnull=True,is_active=True).order_by('-pk')
        return queryset

    def create(self, serializer):
        from datetime import datetime, timedelta
        from shapely.geometry import Point
        from shapely.geometry.polygon import Polygon
        import numpy as np

        user = self.request.user
        data=self.request.data
        if(data):
            # total =float(data['quantity']* data['price'])
            crop = Crop.objects.get(pk=data['master_crop_id'])

            land_id = LandRegistration.objects.get(pk=data['land_id'])

            land_loc = LandLocation.objects.filter(land_id=data['land_id'])
            
            lat=[]
            lon=[]
            for lat_lng in land_loc:
                lat.append(lat_lng.lat)
                lon.append(lat_lng.lon)
            lats_vect = np.array(lat)
            lons_vect = np.array(lon)
            
            lons_lats_vect = np.column_stack((lats_vect,lons_vect)) # Reshape coordinates
            print(lons_lats_vect)
            polygon = Polygon(lons_lats_vect) # create polygon

            pt = Point(float(data['lat']),float(data['long']))
            
            print(polygon.contains(pt))
            # if(polygon.contains(pt)):
            date = datetime.now()
            present = datetime.strftime(date,'%Y-%m-%d')

            try:

                crop_exp = CropRegistration.objects.get(land_id=land_id,is_active=True)
                harvested_date = datetime.strftime(crop_exp.harvested_date,'%Y-%m-%d')
                
                past_crop = Crop.objects.get(pk=crop_exp.master_crop_id_id)
                if(harvested_date < present):
                    
                    crop_exp.is_active=False
                    crop_exp.save()

                    farmer = FarmerPastCrop(
                    past_crop =past_crop.crop_name,
                    past_yield= crop_exp.harvested_date,
                    quantity_in_kg = crop_exp.quantity_in_kg,
                    past_price=crop_exp.expected_price,
                    created_by=user,
                    total_amount=crop_exp.total_amount,
                    land_id=crop_exp.land_id,
                    )
                    farmer.save()
                # return json.Response("Crop Already Added",False)
            except:
                pass;
            crop_count = CropRegistration.objects.filter(land_id=land_id,is_active=True).count()
            if(crop_count>0):
                return json.Response("Crop Already Added",False)

            crop = CropRegistration(
                    planted_date = data['planted_date'],
                    harvested_date= data['harvested_date'],
                    quantity_in_kg = data['quantity'],
                    expected_price = data['expected_price'],
                    land_id = land_id,
                    master_crop_id = crop,
                    unit =data['unit'],
		    crop_desc=data['crop_desc'],
                    is_organic = data['is_organic'],
                    total_amount = data['total_amount'],  
                    created_by=user,
                    user_id = user
                    )
            crop.save()

            if(data['crop_image']):
                for img in data['crop_image']:
                    verified_crop = VerifiedCrop(
                        crop_id = crop,

                        )
                    verified_crop.save()
                    temp_crop_img = TempCropImage(
                        crop_id = crop,
                        img_path = img,
                        crop_lat=data['lat'],
                        crop_long=data['long'],
                        verified_crop_id=verified_crop
                    )
                    temp_crop_img.save()
            return json.Response("success")
            # else:
            #     return json.Response("Location Mismatched",False)

#Get crop details for particular land
class edit_crop_details(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = CropregSerializer
    def get_queryset(self):
        return CropRegistration.objects.filter(deleted_at__isnull=True)

    def perform_update(self, serializer):
        user = self.request.user
        data=self.request.data

        return json.Response("success")

@csrf_exempt
@transaction.atomic
def farmers_land_view(request):
    import json as j
    from math import sin, cos, sqrt, atan2, radians
    R = 6373.0
    land_data = LandRegistration.objects.all()
    location = None;
    land_val = []
    val = []
    # try:
    if land_data is None:
        pass;
    else:
        for land in land_data:
            print(land.id)

            land_va = LandRegistration.objects.get(pk=land.id)
            land_va = django.core.serializers.serialize('json',[land_va])
            land_va = j.loads(land_va)

            land_location = LandLocation.objects.filter(land_id=land.id)
            land_val = []
            for landloc in land_location:

                if location is None:
                    land_location = LandLocation.objects.get(pk=landloc.id)
                    land_location = django.core.serializers.serialize('json',[land_location])
                    land_location = j.loads(land_location)
                    print(land_location)
                    # land_val.append(land_location,"crop":crop)

                    land_val.append(land_location)
                else:
                    #hit when search by location
                    lat2 = radians(float(landloc.lat))
                    lon2 = radians(float(landloc.lon))

                    dlon = lon2 - lon1
                    dlat = lat2 - lat1
                    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
                    c = 2 * atan2(sqrt(a), sqrt(1 - a))

                    distance = R * c

                    print("Result:", distance)

                    if(distance<=float(50)):
                        print("helo")
                        land_location = None;
                        try:
                            land_location = LandLocation.objects.get(pk=landloc.id)
                        except:
                            pass;
                        if land_location is None:
                            land_location=''
                        else:
                            land_location = django.core.serializers.serialize('json',[land_location])
                            land_location = j.loads(land_location)
                            print(land_location)
                            land_val.append(land_location)
                    else:
                        print("no land nearby")

            crop = None;
            crop_detail = None;
            try:
                crop = CropRegistration.objects.get(land_id=land.id,is_active=True)
                crop_detail = Crop.objects.get(pk=crop.master_crop_id_id)
            except:
                pass;
            if crop is None:
                crop='';
                crop_detail="";

            else:
                crop = django.core.serializers.serialize('json',[crop])
                crop = j.loads(crop)
                crop_detail = django.core.serializers.serialize('json',[crop_detail])
                crop_detail = j.loads(crop_detail)


            farmer = Farmer.objects.get(pk=land.farmer_id_id)
            farmer = django.core.serializers.serialize('json',[farmer])
            farmer = j.loads(farmer)
            user = User.objects.get(pk=land.user_id_id)
            user = UserProfile.objects.get(user_id = user.id)
            user = django.core.serializers.serialize('json',[user])
            user = j.loads(user)
            seat = {  "user" : user,"farmer":farmer,"landlocation":land_val,"land":land_va,"crop":crop,"crop_detail":crop_detail}
            if(len(land_val)>0):
                val.append(seat)
            else:
                pass;

        struct = {"data":val}
        # print(struct)

        return json.Response(struct)
    # except:
    #     pass;



#Get list of records based on search
@csrf_exempt
@transaction.atomic
def search_list(request):
    import json as j
    from math import sin, cos, sqrt, atan2, radians
    R = 6373.0

    data = j.loads(request.body.decode('utf-8'))
    print(data)
    if(data['user']=='0'):
        data['user']=''
    land_data = None;
    if(data['farmer']=='' and data['user']=='' and data['lat']==''):
        land_data = LandRegistration.objects.all()
        location = None;
    if(data['farmer']!='' and data['user']=='' and data['lat']==''):
        land_data = LandRegistration.objects.filter(farmer_id=data['farmer'])
        location = None;
    if(data['user']!='' and data['farmer']=='' and data['lat']==''):
        land_data = LandRegistration.objects.filter(user_id=data['user'])
        location = None;
    if(data['farmer']!='' and data['user']!='' and data['lat']==''):
        land_data = LandRegistration.objects.filter(user_id=data['user']).filter(farmer_id=data['farmer'])
        location = None;
    if(data['farmer']=='' and data['user']=='' and data['lat']!='' ):
        land_data = LandRegistration.objects.all()
        lat1 = radians(data['lat'])
        lon1 = radians(data['lng'])
        location =1;
    if(data['farmer']!='' and data['user']=='' and data['lat']!='' ):
        land_data = LandRegistration.objects.filter(farmer_id=data['farmer'])
        lat1 = radians(data['lat'])
        lon1 = radians(data['lng'])
        location =1;

    if(data['farmer']=='' and data['user']!='' and data['lat']!='' ):
        land_data = LandRegistration.objects.filter(user_id=data['user'])
        lat1 = radians(data['lat'])
        lon1 = radians(data['lng'])
        location =1;
    if(data['farmer']!='' and data['user']!='' and data['lat']!='' ):
        land_data = LandRegistration.objects.filter(user_id=data['user']).filter(farmer_id=data['farmer'])
        lat1 = radians(data['lat'])
        lon1 = radians(data['lng'])
        location =1;

    land_val = []
    val = []
    # try:
    if land_data is None:
        pass;
    else:
        for land in land_data:
            print(land.id)

            land_va = LandRegistration.objects.get(pk=land.id)
            land_va = django.core.serializers.serialize('json',[land_va])
            land_va = j.loads(land_va)

            land_location = LandLocation.objects.filter(land_id=land.id)
            land_val = []
            for landloc in land_location:

                if location is None:
                    land_location = LandLocation.objects.get(pk=landloc.id)
                    land_location = django.core.serializers.serialize('json',[land_location])
                    land_location = j.loads(land_location)
                    print(land_location)
                    # land_val.append(land_location,"crop":crop)

                    land_val.append(land_location)
                else:
                    #hit when search by location
                    lat2 = radians(float(landloc.lat))
                    lon2 = radians(float(landloc.lon))

                    dlon = lon2 - lon1
                    dlat = lat2 - lat1
                    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
                    c = 2 * atan2(sqrt(a), sqrt(1 - a))

                    distance = R * c

                    print("Result:", distance)

                    if(distance<=float(50)):
                        print("helo")
                        land_location = None;
                        try:
                            land_location = LandLocation.objects.get(pk=landloc.id)
                        except:
                            pass;
                        if land_location is None:
                            land_location=''
                        else:
                            land_location = django.core.serializers.serialize('json',[land_location])
                            land_location = j.loads(land_location)
                            print(land_location)
                            land_val.append(land_location)
                    else:
                        print("no land nearby")

            crop = None;
            crop_detail = None;
            try:
                crop = CropRegistration.objects.get(land_id=land.id,is_active=True)
                crop_detail = Crop.objects.get(pk=crop.master_crop_id_id)
            except:
                pass;
            if crop is None:
                crop='';
                crop_detail="";

            else:
                crop = django.core.serializers.serialize('json',[crop])
                crop = j.loads(crop)
                crop_detail = django.core.serializers.serialize('json',[crop_detail])
                crop_detail = j.loads(crop_detail)


            farmer = Farmer.objects.get(pk=land.farmer_id_id)
            farmer = django.core.serializers.serialize('json',[farmer])
            farmer = j.loads(farmer)
            user = User.objects.get(pk=land.user_id_id)
            user = django.core.serializers.serialize('json',[user])
            user = j.loads(user)
            seat = {  "user" : user,"farmer":farmer,"landlocation":land_val,"land":land_va,"crop":crop,"crop_detail":crop_detail}
            if(len(land_val)>0):
                val.append(seat)
            else:
                pass;

        struct = {"data":val}
        # print(struct)

        return json.Response(struct)
    # except:
    #     pass;

#get Crop Master List
class crop_master(generics.ListCreateAPIView):
    serializer_class = CropSerializer
    def get_queryset(self):
        from itertools import chain
        others = Crop.objects.filter(crop_name='Other Pulses')
        crop = Crop.objects.filter(is_active=True).order_by('crop_name').exclude(crop_name='Other Pulses')
        
        crop = list(chain(others, crop))
        
        page = self.request.GET.get('page', '')
        # print(page)
        try:
            if(page=='all'):
                others = Crop.objects.filter(crop_name='Other Pulses')
                crop = Crop.objects.filter(is_active=True).order_by('crop_name').exclude(crop_name='Other Pulses')
                
                queryset = list(chain(others, crop))
                # queryset = Crop.objects.all().order_by('crop_name')
            else:
                page =1;
                crop = Paginator(crop, 60)
                queryset = crop.page(page)
        except:
            crop = Paginator(crop, 60)
            queryset = crop.page(1)
        return queryset


#Get farmer list based on user
@csrf_exempt
@transaction.atomic
def load_farmer(request,pk):
    import json as j

    if(pk!='0'):
        farmer = Farmer.objects.filter(user_id=pk)
        farmer = django.core.serializers.serialize('json',farmer)
        farmer = j.loads(farmer)
    else:
        farmer = Farmer.objects.all()

        farmer = django.core.serializers.serialize('json',farmer)
        farmer = j.loads(farmer)
    return json.Response(farmer)

#Crop Master Update
class crop_master_update(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = CropSerializer
    def get_queryset(self):
        return Crop.objects.filter(deleted_at__isnull=True).order_by('pk')

    def perform_update(self, serializer):
        user = self.request.user
        data=self.request.data
        print(data)
        crop = Crop.objects.get(id=data['id'])
        crop.crop_name = data['crop_name']
        crop.tamil_crop_name =data['tamil_crop_name']
        crop.crop_color = data['crop_color']
        crop.save()

        return json.Response("success")

#Register Past crop of farmer
class past_crop(generics.ListCreateAPIView):
    serializer_class = PastCropSerializer
    def get_queryset(self):
        user = self.request.user
        if user.id==1: # for admin
            try:
                return FarmerPastCrop.objects.filter(deleted_at__isnull=True)

            except:
                return FarmerPastCrop.objects.filter(deleted_at__isnull=True).order_by('-pk')

        else: # for user
            return FarmerPastCrop.objects.filter(deleted_at__isnull=True,created_by=user).order_by('-pk')

        
    def create(self, serializer):
       
        from datetime import datetime, timedelta
        import moment
        user = self.request.user
        data=self.request.data
        
        if(data):
            land_id = LandRegistration.objects.get(pk=data['land_id'])
            # curren_date = moment().now().format('YYYY-MM-DD')
            date = datetime.now()
            # print(date)
            # print(datetime.strftime(date,'%Y-%m-%d'))
            # print(datetime.strftime(date,'%Y-%m-%d'))
            # # past=datetime.strftime(date,'%Y-%m-%d')
            past = datetime.strptime(data['past_yield'],'%Y-%m-%d')
            past =datetime.strftime(past,'%Y-%m-%d')
            present = datetime.strftime(date,'%Y-%m-%d')
            try:
                past_crop = Crop.objects.get(pk=data['past_crop'])
            except:
                past_crop = data['past_crop']
            if(past>present):
                return json.Response("Yield date is not valid",False)
            farmer = FarmerPastCrop(
                    past_crop =past_crop.crop_name,
                    past_crop_type=data['crop_sub_type'],
                    past_yield= data['past_yield'],
                    quantity_in_kg = data['quantity_in_kg'],
                    past_price=data['past_price'],
                    created_by=user,
                    total_amount=float(float(data['quantity_in_kg'])*float(data['past_price'])),
                    land_id=land_id,
                    )
            farmer.save()

            return json.Response("success")
        else:
            return json.Response("Data Cannot be empty",False)


class land_filter(generics.ListAPIView):
    serializer_class = LandLocationSerializer
    def get_queryset(self):
        # print(self.request.query_params.get('from_date'));
        from_date = self.request.query_params.get('from_date')
        to_date = self.request.query_params.get('to_date')
        print(to_date)
        print(from_date)
        land = LandRegistration.objects.filter(Q(created_on__gte=from_date)).filter(Q(created_on__lte=to_date))
        # print(len(crop))
        # land_val=[]
        if(len(land)>0):

            # for land in crop:
            #     print(land.land_id_id)
            #     land = LandRegistration.objects.filter(pk=land.land_id_id)
            #     land_val.append(land)

            return land
        else:
            return
            # return json.Response("No Data Found",False)
        # return land_val[0]

# Report based on harvest date
@csrf_exempt
@transaction.atomic
def report(request):
    import json as j
    if request.method == 'POST':
        data = j.loads(request.body.decode('utf-8'))
        print(data)
        print(data['from_date'])
        crop = CropRegistration.objects.filter(Q(harvested_date__gte=data['from_date'])).filter(Q(harvested_date__lte=data['to_date'])).filter(is_active=True)
        data=[]
        for crop_data in crop:

            crop = CropRegistration.objects.get(pk=crop_data.id)
            crop_master = Crop.objects.get(pk=crop.master_crop_id_id)
            land = LandRegistration.objects.get(pk = crop.land_id_id)
            farmer = Farmer.objects.get(pk=land.farmer_id_id)
            crop = django.core.serializers.serialize('json',[crop])
            crop = j.loads(crop)

            land = django.core.serializers.serialize('json',[land])
            land = j.loads(land)

            land_loc = LandLocation.objects.filter(land_id=land[0]['pk'])
            land_loc = django.core.serializers.serialize('json',land_loc)
            land_loc = j.loads(land_loc)
            land = {"land":land[0],"land_loc":land_loc}
            crop_master = django.core.serializers.serialize('json',[crop_master])
            crop_master = j.loads(crop_master)

            farmer = django.core.serializers.serialize('json',[farmer])
            farmer = j.loads(farmer)

            crop = {"crop":crop[0],"land":land,"crop_details":crop_master[0],"farmer":farmer[0]}
            print(crop)
            data.append(crop)
        return json.Response(data)

@csrf_exempt
@transaction.atomic
def get_lang(request):
    import json as j
    data = j.loads(request.body.decode('utf-8'))
    print(data['lang'])
    json_data = open('config/media/en.json')   
    data1 = j.load(json_data) 
    # data2 = json.dumps(json_data) 

    json_data.close()
    return json.Response(data1)


@csrf_exempt
@transaction.atomic
def crop_update(request):
    import json as j
    data = j.loads(request.body.decode('utf-8'))
    print(data)

    #crop = CropRegistration.objects.filter(pk=data['crop_id']).update(master_crop_id=data['master_crop'])
    # json_data = open('config/media/en.json')   
    # data1 = j.load(json_data) 
    # # data2 = json.dumps(json_data) 

    # json_data.close()
    return json.Response("Crop Updated")


#CSV Download
@csrf_exempt
@transaction.atomic
def csv(request):
    import csv
    from django.utils.encoding import smart_str

    # with open('names.csv', 'w') as csvfile:

    response = HttpResponse(content_type='text/csv')
    #decide the file name
    response['Content-Disposition'] = 'attachment; filename="ThePythonDjango.csv"'

    writer = csv.writer(response, csv.excel)
    response.write(u'\ufeff'.encode('utf8'))

    #write the headers
    writer.writerow([
        smart_str(u"Event Name"),
        smart_str(u"Start Date"),
        smart_str(u"End Date"),
        smart_str(u"Notes"),
    ])
    #get data from database or from text file....
    events =[{"name":"abi","start_date_time":"sdf","end_date_time":"asd","notes":"notes"}] #dummy function to fetch data
    for event in events:
        writer.writerow([
            smart_str(event['name']),
            smart_str(event['start_date_time']),
            smart_str(event['end_date_time']),
            smart_str(event['notes']),
        ])
    return response


def remove_image(request):
    import json as j
    if request.method == 'POST':
        data = j.loads(request.body.decode('utf-8'))
        print(data)
        print(data['image_id'])
        temp_img = TempCropImage.objects.filter(Q(id__in=[o for o in data['image_id']]))
        temp_img.delete()
        return json.Response("success")
  

@csrf_exempt
@transaction.atomic
def get_land(request):
    import json as j
    from math import sin, cos, sqrt, atan2, radians
    R = 6373.0
    try:
        auth = AuthToken.objects.get(apiKey=helpers.header(request))
    except:
        return json.Response("Authentication Error")
    # print(auth.user_id)
    # print("fsdf")
    try:
        page = request.get('page', '')
        land_data = LandRegistration.objects.filter(deleted_at__isnull=True).exclude(user_id_id=auth.user_id)
        land_data = Paginator(land_data, 10)
        land_data = land_data.page(page)
        # return queryset
    except:
        land_data = LandRegistration.objects.filter(deleted_at__isnull=True).exclude(user_id_id=auth.user_id)
         
    # print(land_data)
    location = 1;
    land_val = []
    val = []
    if request.method == 'POST':
        data = j.loads(request.body.decode('utf-8'))
    print(data['lat'])
    lat1 = radians(data['lat'])
    lon1 = radians(data['lng'])
    # try:
    if land_data is None:
        pass;
    else:
        for land in land_data:
            print(land.id)

            land_va = LandRegistration.objects.get(pk=land.id)
            land_va = django.core.serializers.serialize('json',[land_va])
            land_va = j.loads(land_va)

            land_location = LandLocation.objects.filter(land_id=land.id)
            land_val = []
            for landloc in land_location:

                if location is None:
                    land_location = LandLocation.objects.get(pk=landloc.id)
                    land_location = django.core.serializers.serialize('json',[land_location])
                    land_location = j.loads(land_location)
                    print(land_location)
                    # land_val.append(land_location,"crop":crop)

                    land_val.append(land_location)
                else:
                    #hit when search by location
                    lat2 = radians(float(landloc.lat))
                    lon2 = radians(float(landloc.lon))

                    dlon = lon2 - lon1
                    dlat = lat2 - lat1
                    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
                    c = 2 * atan2(sqrt(a), sqrt(1 - a))

                    distance = R * c

                    print("Result:", distance)

                    if(distance<=float(50)):
                        print("helo")
                        land_location = None;
                        try:
                            land_location = LandLocation.objects.get(pk=landloc.id)
                        except:
                            pass;
                        if land_location is None:
                            land_location=''
                        else:
                            land_location = django.core.serializers.serialize('json',[land_location])
                            land_location = j.loads(land_location)
                            
                            print(land_location)
                            land_val.append(land_location)
                    else:
                        print("no land nearby")

            crop = None;
            crop_detail = None;
            try:
                crop = CropRegistration.objects.get(land_id=land.id,is_active=True)
                crop_detail = Crop.objects.get(pk=crop.master_crop_id_id)
            except:
                pass;
            if crop is None:
                crop=''
                crop_detail=''

            else:
                crop = django.core.serializers.serialize('json',[crop])
                crop = j.loads(crop)
                crop_detail = django.core.serializers.serialize('json',[crop_detail])
                crop_detail = j.loads(crop_detail)

            if(crop_detail!='' or crop!='' ):
                crop_detail = crop_detail
                crop = crop
                
            farmer = Farmer.objects.get(pk=land.farmer_id_id)
            farmer = django.core.serializers.serialize('json',[farmer])
            farmer = j.loads(farmer)
            user = User.objects.get(pk=land.user_id_id)
            user = UserProfile.objects.get(user_id = user.id)
            user = django.core.serializers.serialize('json',[user])
            user = j.loads(user)
            seat = {  "user" : user,"farmer":farmer,"landlocation":land_val,"land":land_va,"crop":crop,"crop_detail":crop_detail}
            if(len(land_val)>0):
                val.append(seat)
            else:
                pass;

        struct = {"data":val}
        # print(struct)

        return json.Response(struct)
    # except:
    #     pass;

# @csrf_exempt
# @transaction.atomic
# def get_surveyor_land(request):
#     import json as j
#     try:
#         auth = AuthToken.objects.get(apiKey=helpers.header(request))
#     except:
#         return json.Response("Authentication Error")
#     # print(auth.user_id)
#     # print("fsdf")
#     try:
#         page = request.get('page', '')
#         land_data = LandRegistration.objects.filter(deleted_at__isnull=True,user_id_id=auth.user_id)
#         land_data = Paginator(land_data, 10)
#         land_data = land_data.page(page)
#         # return queryset
#     except:
#         land_data = LandRegistration.objects.filter(deleted_at__isnull=True,user_id_id=auth.user_id)
         
#     print(land_data)
    
#     land_val = []
#     val = []
    
#     # try:
#     if land_data is None:
#         pass;
#     else:
#         for land in land_data:
#             print(land.id)

#             land_va = LandRegistration.objects.get(pk=land.id)
#             land_va = django.core.serializers.serialize('json',[land_va])
#             land_va = j.loads(land_va)

#             land_location = LandLocation.objects.filter(land_id=land.id)
#             land_val = []
#             for landloc in land_location:

                
#                 land_location = LandLocation.objects.get(pk=landloc.id)
#                 land_location = django.core.serializers.serialize('json',[land_location])
#                 land_location = j.loads(land_location)
#                 print(land_location)
#                 # land_val.append(land_location,"crop":crop)

#                 land_val.append(land_location)
                
#             crop = None;
#             crop_detail = None;
#             try:
#                 crop = CropRegistration.objects.get(land_id=land.id)
#                 crop_detail = Crop.objects.get(pk=crop.master_crop_id_id)
#             except:
#                 pass;
#             if crop is None:
#                 crop=''
#                 crop_detail=''

#             else:
#                 crop = django.core.serializers.serialize('json',[crop])
#                 crop = j.loads(crop)
#                 crop_detail = django.core.serializers.serialize('json',[crop_detail])
#                 crop_detail = j.loads(crop_detail)

#             if(crop_detail!='' or crop!='' ):
#                 crop_detail = crop_detail
#                 crop = crop 
                
#             farmer = Farmer.objects.get(pk=land.farmer_id_id)
#             farmer = django.core.serializers.serialize('json',[farmer])
#             farmer = j.loads(farmer)
#             user = User.objects.get(pk=land.user_id_id)
#             user = UserProfile.objects.get(user_id = user.id)
#             user = django.core.serializers.serialize('json',[user])
#             user = j.loads(user)
#             seat = {  "user" : user,"farmer":farmer,"landlocation":land_val,"land":land_va,"crop":crop,"crop_detail":crop_detail}
#             if(len(land_val)>0):
#                 val.append(seat)
#             else:
#                 pass;

#         struct = {"data":val}
#         # print(struct)

#         return json.Response(struct)
#     # except:
#     #     pass;


@csrf_exempt
@transaction.atomic
def get_surveyor_land(request):
    import json as j
    try:
        auth = AuthToken.objects.get(apiKey=helpers.header(request))
    except:
        return json.Response("Authentication Error")
    # requestData = json.ParseData(request.user)
   #check whether user is authenticated
    
    try:
        farmers = Farmer.objects.filter(deleted_at__isnull=True,user_id_id=auth.user_id)
        
    except:
        farmers = Farmer.objects.filter(deleted_at__isnull=True,user_id_id=auth.user_id)
         
    # print(farmers)
    
    land_val = []
    val = []
    farmer_data = django.core.serializers.serialize('json',farmers)
    farmer_data = j.loads(farmer_data)
    # print(farmer_data)
    user = User.objects.get(pk=auth.user_id)
    user = UserProfile.objects.get(user_id = user.id)
    user = django.core.serializers.serialize('json',[user])
    user_info = j.loads(user)
    # user_info.append(user[0])
    # try:
    land_data = []
    crop_data = []
    crop_info=[]
    
    if farmers is None:
        pass;
    else:
        for farmer in farmers:
            
            # print(farmer.id)
            land_va = LandRegistration.objects.filter(farmer_id=farmer.id)

            for land in land_va:
                land_va = LandRegistration.objects.filter(pk=land.id)
                land_location = LandLocation.objects.filter(land_id=land.id)
                land_val = []

                for landloc in land_location:

                    
                    land_location = LandLocation.objects.get(pk=landloc.id)
                    land_location = django.core.serializers.serialize('json',[land_location])
                    land_location = j.loads(land_location)
                    # print(land_location)
                    # land_val.append(land_location,"crop":crop)

                    land_val.append(land_location[0])



                land_va = django.core.serializers.serialize('json',land_va)
                land_va = j.loads(land_va)
                # print(land_va)
                ld = {"land":land_va[0],"land_location":land_val}
                land_data.append(ld)
                

                crop = None;
                crop_detail = None;
                try:
                    crop = CropRegistration.objects.get(land_id=land.id,is_active=True)
                    crop_detail = Crop.objects.get(pk=crop.master_crop_id_id)
                except:
                    pass;
                if crop is None:
                    crop=''
                    crop_detail=''
                   
                else:
                    crop = django.core.serializers.serialize('json',[crop])
                    crop = j.loads(crop)
                    crop_detail = django.core.serializers.serialize('json',[crop_detail])
                    crop_detail = j.loads(crop_detail)
                    crop_data.append(crop[0])
                    crop_info.append(crop_detail[0])

            print(request.GET.get('farmer'))
            if(request.GET.get('farmer')=='1'):
                print("sdf")
                seat = {"Farmer":farmer_data}
            if(request.GET.get('crop')=='1'):
                seat = {"crop":crop_data,"crop_master":crop_info}
            if(request.GET.get('land')=='1'):    
                seat = {"land":land_data}
            if(request.GET.get('farmer')==request.GET.get('crop')==request.GET.get('land')==None):       
                seat = {"Farmer":farmer_data,"land":land_data,"crop":crop_data,"crop_master":crop_info,"user":user_info[0]}
            if(len(farmer_data)>0):
                val.append(seat)
            else:
                val.append(seat)

        # struct = {"data":val}
        # print(struct)
        try:
            if(val[0]):

                return json.Response(val[0])
        except:
            
            return json.Response({"Farmer":[],"land":[],"crop":[],"crop_master":[],"user":{}})
    # except:
    #     pass;

@csrf_exempt
@transaction.atomic
def buyer_list(request):
    from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

    import json as j
    from math import sin, cos, sqrt, atan2, radians
    R = 6373.0
    # if request.method == 'GET':
    try:
        data = j.loads(request.body.decode('utf-8'))
        print(data);
    except:
        pass;
    try:
        auth = AuthToken.objects.get(apiKey=helpers.header(request))
    except:
        return json.Response("Authentication Error")
    print(auth.user_id)
    try:
        harvest_date = data['date']
    except:
        harvest_date = ''
    try:
        crop_name = data['crop']
    except:
        crop_name = ''
    location = None
    try: 
        page = data['page']
        print(page)
    except:
        page = 1
    try:
        distance = data['lat']
        lat1 = radians(data['lat'])
        lon1 = radians(data['lng'])
        
    except:
        distance = ''
        lat1=''
        lon1=''

    if(harvest_date and crop_name and distance):
        location = 1;
        crop =  CropRegistration.objects.filter((Q(master_crop_id__crop_name__icontains = crop_name) & Q(harvested_date__gte = harvest_date))).filter(is_active=True)

       
    else:
        if(harvest_date and (crop_name=='') and (distance=='') ):
            print("helo1")
            crop =  CropRegistration.objects.filter( (Q(harvested_date__gte = harvest_date))).filter(is_active=True)
            print(crop)
        if((harvest_date=='') and crop_name and (distance=='')):
            print("helo2")
            crop =  CropRegistration.objects.filter( (Q(master_crop_id__crop_name__icontains = crop_name ))).filter(is_active=True)
        if((harvest_date=='') and (crop_name=='') and distance):
            print("helo3")
            location = 1;
            crop =  CropRegistration.objects.filter(is_active=True)

        if(harvest_date and crop_name and (distance=='')):
            print("helo4")
            crop =  CropRegistration.objects.filter((Q(master_crop_id__crop_name__icontains = crop_name ) & Q(harvested_date__gte = harvest_date))).filter(is_active=True)

        if(harvest_date and (crop_name=='') and distance):
            print("helo5")
            location = 1;
            crop =  CropRegistration.objects.filter( (Q(harvested_date__gte = harvest_date)) ).filter(is_active=True)
        if((harvest_date=='') and crop_name and distance):
            print("helo6")
            location = 1;
            crop =  CropRegistration.objects.filter((Q(master_crop_id__crop_name__icontains = crop_name ) ) ).filter(is_active=True)
        if((harvest_date=='') and (crop_name=='') and (distance=='') ):
            crop =  CropRegistration.objects.filter( is_active=True)

    # crop = django.core.serializers.serialize('json',crop)
    # crop = j.loads(crop)
    # print(crop)
    # return json.Response(crop)
    land_val = []
    val = []
    farmer_data = []
    
    
    # # try:
    land_data = []
    crop_data = []
    crop_info=[]
    
    if len(crop) <1:
        
        return json.Response(val)
    else:
        for crops in crop:
            
            # print(crop.id)
            land_va = LandRegistration.objects.filter(pk=crops.land_id_id)
            user = User.objects.get(pk=crops.user_id_id)
            user = UserProfile.objects.get(user_id = user.id)
            user = django.core.serializers.serialize('json',[user])
            user_info = j.loads(user)
            # user_info.append(user[0])
            for land in land_va:
                land_va = LandRegistration.objects.filter(pk=land.id)
                land_location = LandLocation.objects.filter(land_id=land.id)
                land_val = []
                distance_km = []
                for landloc in land_location:

                    
                    # land_location = LandLocation.objects.get(pk=landloc.id)
                    # land_location = django.core.serializers.serialize('json',[land_location])
                    # land_location = j.loads(land_location)
                    # print(land_location)
                    # # land_val.append(land_location,"crop":crop)

                    # land_val.append(land_location[0])

                    if location is None:
                        land_location = LandLocation.objects.get(pk=landloc.id)
                        land_location = django.core.serializers.serialize('json',[land_location])
                        land_location = j.loads(land_location)
                        print(land_location)
                        # land_val.append(land_location,"crop":crop)

                        land_val.append(land_location[0])
                    else:
                        #hit when search by location
                        lat2 = radians(float(landloc.lat))
                        lon2 = radians(float(landloc.lon))

                        dlon = lon2 - lon1
                        dlat = lat2 - lat1
                        a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
                        c = 2 * atan2(sqrt(a), sqrt(1 - a))

                        distance_val = R * c
                        distance_val = round(distance_val,2)
                        distance_km.append(distance_val)
                        print("Result:", distance_val)

                        if(distance_val<=float(50)):
                            print("helo")
                            land_location = None;
                            try:
                                land_location = LandLocation.objects.get(pk=landloc.id)
                            except:
                                pass;
                            if land_location is None:
                                land_location=''
                            else:
                                land_location = django.core.serializers.serialize('json',[land_location])
                                land_location = j.loads(land_location)
                                
                                print(land_location)
                                land_val.append(land_location[0])
                        else:
                            print("no land nearby")
                if(len(land_val)>0):
                    land_va = django.core.serializers.serialize('json',land_va)
                    land_va = j.loads(land_va)
                    print(land_va)
                    ld = {"land":land_va[0],"land_location":land_val}
                    land_data.append(ld)
                    print(land.farmer_id_id)
                    farmer = None;
                    try:
                        farmer = Farmer.objects.get(pk=land.farmer_id_id)
                    except:
                        pass;
                    if farmer is None:
                        pass;
                    else:
                        farmer = django.core.serializers.serialize('json',[farmer])
                        farmer = j.loads(farmer)
                       
                        # farmer_data.append(farmer[0])
                    # crop = None;
                    # crop_detail = None;
                    # try:
                    #     crop = CropRegistration.objects.get(land_id=land.id)
                    #     crop_detail = Crop.objects.get(pk=crop.master_crop_id_id)
                    # except:
                    #     pass;
                    if crop is None:
                        crop=''
                        crop_detail=''
                       
                    else:
                        try:
                            crop = CropRegistration.objects.get(pk=crops.id,is_active=True)
                            crop_detail = Crop.objects.get(pk=crops.master_crop_id_id)
                            crop = django.core.serializers.serialize('json',[crop])
                            crop = j.loads(crop)

                            crop_detail = django.core.serializers.serialize('json',[crop_detail])
                            crop_detail = j.loads(crop_detail)
                            crop_images = None
                            try:
                                crop_images = TempCropImage.objects.filter(crop_id=crops.id)
                            except:
                                pass;

                            if crop_images is None:
                                crop_images=[]
                            else:
                                crop_images = django.core.serializers.serialize('json',crop_images)
                                crop_images = j.loads(crop_images)
                            # crop_data.append(crop[0])
                            # crop_info.append(crop_detail[0])
                            seat={"crop":crop,"crop_master":crop_detail,"crop_images":crop_images,"land_location":land_val,"land":land_va[0],"farmer":farmer[0],"user":user_info[0],"distance":distance_val}
                            val.append(seat)  
                        except:
                            pass;
                # seat = {"Farmer":farmer_data,"land":land_data,"crop":crop_data,"crop_master":crop_info,"user":user_info[0]}
                # if(len(val)>0):
                #     # val.append(seat)
                # else:
                #     # val.append(seat)
                    

        # struct = {"data":val}
        # print(struct)
        try:
            if(len(val)>0):
                # page =1;
                print(page)
                paginator = Paginator(val, 100)
                try:

                    users = paginator.page(page)
                except PageNotAnInteger:
                # If page is not an integer, deliver first page.
                    users = paginator.page(1)
                except EmptyPage:
                # If page is out of range (e.g. 7777), deliver last page of results.
                    users = paginator.page(paginator.num_pages)
                # val = Paginator(val, 10)
                # val = val.page(page)
                return json.Response(list(users))
            else:
                return json.Response(val)

        except:
            
            return json.Response(val)
    # except:
    #     pass;
@csrf_exempt
@transaction.atomic
def get_crop_count(request):
    import json as j
    from math import sin, cos, sqrt, atan2, radians
    from django.db.models import Count
    from collections import Counter
    import collections

    R = 6373.0
   
    try:
        auth = AuthToken.objects.get(apiKey=helpers.header(request))
    except:
        return json.Response("Authentication Error")
    try:
        data = j.loads(request.body.decode('utf-8'))
        distance = data['lat']
        lat1 = radians(data['lat'])
        lon1 = radians(data['lng'])
        location=1
    except:
        location = None
        distance = ''
        lat1=''
        lon1=''
    t=[]

    crop_data=[]
    crop = CropRegistration.objects.filter(is_active=True)
    
    for crops in crop:
            # print(crop.id)
            land_va = LandRegistration.objects.filter(pk=crops.land_id_id)

            for land in land_va:
                land_va = LandRegistration.objects.filter(pk=land.id)
                land_location = LandLocation.objects.filter(land_id=land.id)
                land_val = []
                distance_km = []
                for landloc in land_location:
                    if location is None:
                        land_location = LandLocation.objects.get(pk=landloc.id)
                        land_location = django.core.serializers.serialize('json',[land_location])
                        land_location = j.loads(land_location)
                        
                        land_val.append(land_location[0])
                    else:
                        #hit when search by location
                        lat2 = radians(float(landloc.lat))
                        lon2 = radians(float(landloc.lon))

                        dlon = lon2 - lon1
                        dlat = lat2 - lat1
                        a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
                        c = 2 * atan2(sqrt(a), sqrt(1 - a))

                        distance_val = R * c
                        distance_val = round(distance_val,2)
                        distance_km.append(distance_val)
                        print("Result:", distance_val)

                        if(distance_val<=float(50)):
                            print("helo")
                            print(landloc.land_id_id)
                            crop = None;  
                            
                        else:
                            crop = True;

                            print("no land nearby")
            if crop is None:
                try:
                    crop = CropRegistration.objects.get(land_id=landloc.land_id,is_active=True)
                    crop_data_val = django.core.serializers.serialize('json',[crop])
                    crop_data_val = j.loads(crop_data_val)
                    
                    
                    crop_data.append(crop_data_val[0]['fields']['master_crop_id'])
                except:
                    pass;
            
    if(len(crop_data)>0):            
        total_crop = len(crop_data)                       
        uniqueList = []
        t=[]
        for c in crop_data:
            single_occurrences = collections.Counter(crop_data)
            # print(single_occurrences.values())
            
            # print('%s : %d' % (c, single_occurrences[c]))
            crop_list = Crop.objects.values('id','crop_name','crop_color','tamil_crop_name').filter(pk=c)              
            
            for crop in crop_list:
                crop['master_crop_id__crop_name']=crop['crop_name']
                crop['master_crop_id__tamil_crop_name']=crop['tamil_crop_name']
                crop['master_crop_id__crop_color']=crop['crop_color']

                crop['per'] = single_occurrences[c] * 100 / total_crop
                crop['per'] = round(crop['per'],2)
                t.append(crop)
        # print(t)
        # list_set = set(t)
        
        unique_data = [dict(t1) for t1 in set([tuple(d.items()) for d in t])]

        sorted_list = sorted(unique_data, key=lambda k: (-k['per']))

        return json.Response(sorted_list[0:10],True) 
    else:
        return json.Response("No Data found",False)
    # return json.Response(crop_data,True);

    # total_crop = CropRegistration.objects.all().count()
    # total_val = CropRegistration.objects.values('master_crop_id__crop_name','master_crop_id__crop_color','master_crop_id__tamil_crop_name').annotate(total=Count('master_crop_id__crop_name')).order_by('total')[:10]
    
    # crop_list = list(total_val)
    # for crop in crop_list:
        
    #     crop['per'] = crop['total'] * 100 / total_crop
    #     crop['per'] = round(crop['per'],2)
    #     t.append(crop)
    
    # return json.Response(t)
       

class captcha_view(generics.ListCreateAPIView):
    serializer_class = LandLocationSerializer
    def get_queryset(self):
        import random
        foo = ['a', 'b', 'c', 'd', 'e']
        print(random.choice(foo))
        return json.Response(random.choice(foo))

    def get(self, request):
        import random
        import string
        import json as j
        user = self.request.user
        crop = CropRegistration.objects.all()
        crop_data_val=[]
        if crop:
            for crops in crop:
                print(crop)
                crop = django.core.serializers.serialize('json',[crops])
                crop = j.loads(crop)
                
                crop_data = Crop.objects.filter(pk=crop[0]['fields']['master_crop_id'])
                crop_detail = django.core.serializers.serialize('json',crop_data)
                crop_detail = j.loads(crop_detail)
                print(crop_detail[0])
                crop_name = crop_detail[0]['fields']['crop_name']
                crop_img = None;
                try:
                    crop_img = TempCropImage.objects.filter(crop_id=crop[0]['pk'])
                    
                except:
                    pass;
                if crop_img is None:
                    pass;
                else:
                    crop_img = django.core.serializers.serialize('json',crop_img)
                    crop_img = j.loads(crop_img)
                if crop_img:

                    crop_data_val.append(crop_img[0])

            # print(crop_name)
                #identify one crop name for question
            foo = [{'a':'1'},{'b':'2'}, {'c':'3'}, {'d':'4'}, {'e':'5'}]
            print(random.choice(crop_data_val))
            v=4
            string = [random.choice(crop_data_val) for _ in range(v)]
            for ran_img in string:
                
                ran_img = TempCropImage.objects.get(pk=ran_img['pk'])
                verify = TempVerify(
                user_id = user,
                temp_crop_id = ran_img,
                is_active=True,
                )
                verify.save()
                # print(ran_img.crop_id)
                crop_data = Crop.objects.get(pk=ran_img.crop_id.master_crop_id.pk)
                crop_detail = django.core.serializers.serialize('json',[crop_data])
                crop_detail = j.loads(crop_detail)
                print(crop_detail[0])
                crop_name = crop_detail[0]['fields']

            data = {"images":string,"question":"Please choose the correct crop ","crop":crop_name }
            return json.Response(data)
        else:
            return json.Response("No Data Found",False)


class captcha_verify(generics.ListCreateAPIView):
    def get_queryset(self):
        user = self.request.user
        if user.id==1:
            # for admin
            return TempVerify.objects.filter(deleted_at__isnull=True)

        else:
            # for user
            return TempVerify.objects.filter(deleted_at__isnull=True,created_by=user.id)

    def post(self, request):
        import json as j
        user = self.request.user
        data = self.request.data
        # print(data)
        
        random_img = TempVerify.objects.filter(is_active=True,user_id=user.id)
        img=[]
        for sel_img in random_img:
            captcha_crop = django.core.serializers.serialize('json',[sel_img])
            captcha_crop = j.loads(captcha_crop)
            print(captcha_crop)
            img.append(captcha_crop[0]['fields']['temp_crop_id'])
            # print(sel_img.temp_crop_id)
            # if data:
        selected_img = data
        
        for img_array in data:
            print(img_array)
            
            print(img)
            if(img_array['pk'] in img):
                temp_crop = TempCropImage.objects.get(pk=img_array['pk'])
                temp_crop = django.core.serializers.serialize('json',[temp_crop])
                temp_crop = j.loads(temp_crop)
                verify_crop = VerifiedCrop.objects.get(pk=temp_crop[0]['fields']['verified_crop_id'])
                # verify_crop = VerifiedCrop.objects.get(pk=temp_crop['verified_crop_id'])
                verify_crop.success_count = int(verify_crop.success_count) + int(1);
                verify_crop.save()
                temp_verify = TempVerify.objects.filter(temp_crop_id=img_array['pk'],user_id=user.id).update(is_active=False)
            else:
                temp_verify = TempVerify.objects.filter(temp_crop_id=img_array['pk'],user_id=user.id).update(is_active=False)
                
                temp_crop = TempCropImage.objects.get(pk=img_array['pk'])
                temp_crop = django.core.serializers.serialize('json',[temp_crop])
                temp_crop = j.loads(temp_crop)
                verify_crop = VerifiedCrop.objects.get(pk=temp_crop[0]['fields']['verified_crop_id'])
              
                verify_crop.failure_count = int(verify_crop.failure_count) + int(1);
                verify_crop.save()
        return  json.Response("Crop Verified")  


class contact_farmer(generics.ListCreateAPIView):
    serializer_class = FarmerSerializer
    def get_queryset(self):
        import random
        user = self.request.user
        if user.id==1:
            # for admin
            return Farmer.objects.filter(deleted_at__isnull=True)

        else:
            # for user
            return Farmer.objects.filter(deleted_at__isnull=True,created_by=user)


    def post(self, request):
        user = self.request.user
        data = self.request.data
        print(data)
        print(user)
        user = User.objects.get(pk=user.id)
        user_profile = UserProfile.objects.filter(user_id=user).update(is_buyer=True)
        user_phone = UserProfile.objects.get(user_id=user)
        mob_number = user_phone.phone_no
        print(user_profile)
        farmer = Farmer.objects.get(pk=data['farmer_id'])
        farmer_name = farmer.name
        farmer_number = farmer.phone_no
        msg = "Sandhai: Buyer is trying to reach you. Contact no:{f_number}".format(name=user_phone.name,f_number=mob_number)
        err = "Something went wrong message sending"
        #print(msg)
        check = sms_push(farmer_number,msg)
        if (check == True):
            contact = UserContact(
                user_id = user,
                farmer_id = farmer,
                )
            contact.save()
            return json.Response("success")
        else:
            return json.Response({"send_error":err})

# Datas import directly to database (for Dev use)
class excel_upload(generics.ListCreateAPIView):
    serializer_class = LandLocationSerializer
    def get_queryset(self):
        return LandRegistration.objects.filter(deleted_at__isnull=True)

    def post(self, request):
            handle_uploaded_file(request, request.FILES['fileKey'])

def handle_uploaded_file(request, f):
    import xlrd
    from django.db.models import DecimalField
    book = xlrd.open_workbook(file_contents=f.read())

    # crop upload
    for sheet in book.sheets():
        number_of_rows = sheet.nrows
        number_of_columns = sheet.ncols
        print(number_of_rows)
        for row in range(1, number_of_rows):
            # pass;
            # crop Import
            crop_name = (sheet.cell(row, 1).value)
            tamil_crop = (sheet.cell(row, 2).value)
            crop_color = (sheet.cell(row, 3).value)
            crop = Crop(
                crop_name=crop_name,
                tamil_crop_name=tamil_crop,
                crop_color = crop_color,
            )
            crop.save()

            # land import
            # user_id = (sheet.cell(row,1).value)
            # farmer_id = (sheet.cell(row,2).value)
            # land_name=(sheet.cell(row,3).value)
            # land_address=(sheet.cell(row,4).value)
            # land = LandRegistration(
            #     land_name=land_name,
            #     land_address=land_address,
            #     farmer_id_id=farmer_id,
            #     user_id_id=user_id,
            #     )
            # land.save()


            #landlocation

            # land_id =  (sheet.cell(row,1).value)
            # lat =  (sheet.cell(row,2).value)
            # lng =  (sheet.cell(row,3).value)

            # land_loc = LandLocation(
            #     land_id_id = land_id,
            #     lat = lat,
            #     lon=lng,
            #     )
            # land_loc.save()

            # crop_reg

            # user_id =  (sheet.cell(row,1).value)
            # land_id =  (sheet.cell(row,2).value)
            # crop_id =  (sheet.cell(row,3).value)
            # plant_date = (sheet.cell(row,4).value)
            # harvest_date = (sheet.cell(row,5).value)
            # qty = (sheet.cell(row,6).value)
            # exp_price =(sheet.cell(row,7).value)
            # total = (sheet.cell(row,8).value)

            # crop = CropRegistration(
            #     master_crop_id_id = crop_id,
            #     user_id_id = user_id,
            #     land_id_id = land_id,
            #     planted_date ='2017-04-07',
            #     harvested_date =  '2018-04-07',
            #     quantity_in_kg = 2,
            #     expected_price = 100,
            #     total_amount = 200,

            #     )
            # crop.save()
