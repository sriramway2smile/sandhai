import json,geocoder
from django.http import request
from .models import *
from .serializer import *
from django.contrib.auth import authenticate, login
from rest_framework_jwt.settings import api_settings
import pyqrcode,uuid,math

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

# earth_radius = 3960.0  # for miles
earth_radius = 6371.0  # for kms
degrees_to_radians = math.pi/180.0
radians_to_degrees = 180.0/math.pi

def change_in_latitude(distance):
    "Given a distance north, return the change in latitude."
    return (distance/earth_radius)*radians_to_degrees

def change_in_longitude(latitude, distance):
    "Given a latitude and a distance west, return the change in longitude."
    # Find the radius of a circle around the earth at given latitude.
    r = earth_radius*math.cos(latitude*degrees_to_radians)
    return (distance/r)*radians_to_degrees

def bounding_box(latitude, longitude, distance):
    distance=float(distance)
    lat_change = change_in_latitude(distance)
    print(lat_change)
    lat_max = latitude + lat_change
    lat_min = latitude - lat_change
    lon_change = change_in_longitude(latitude, distance)
    lon_max = longitude + lon_change
    lon_min = longitude - lon_change
    return (lon_max, lon_min, lat_max, lat_min)

def expect_urls():
    urls = [
    '/api/auth/',
    '/api/auth-refresh/',
    '/api/dev-roles/',
    '/api/login/',
    '/api/upload/',
    '/api/sample/',
    '/api/register/',
    '/api/sms_push/',
    '/api/test_layout/',
    '/api/user_profile_update/',
    '/api/content_update/',
    '/api/users/',
    '/api/forgot/',
    '/api/update_password/',
    '/api/add_farmer/',
    '/api/profile/'
    '/api/csv/',
    '/api/userlogin/',
    '/api/search_list/',
    '/api/crop_master/',
    '/api/load_farmer/',
    '/api/auth_user/',
    '/api/paytm/',
    '/api/land_registration/',
    '/api/farmers_land_view/',
    '/api/check_user',
    '/api/land_filter/',
    '/api/past_crop/',
    '/api/get_lang/',
    '/api/crop_update/',
    '/api/get_land/',
    '/api/get_surveyor_land/',
    '/api/buyer_list/',
    '/api/captcha_view/',
    '/api/captcha_verify/',
    '/api/contact_farmer/',
    '/api/get_count/',
    '/api/buyer/',
    '/api/add_crop/'
    ]
    return urls

def relative_urls():
    urls = [
        
       "(\/api\/reset_pwd\/)(?P<string>.+)$",
       "(\/media\/)",
    ]
    return urls

def header(request):
    auth = request.META.get('HTTP_AUTHORIZATION')
    if auth:
        auth = auth.split()
    if auth and auth[1]:
        return auth[1]

    return False;

def client(request):
    client = request.META.get('HTTP_X_CLIENT_DATA')
    if not client:
        return False
    return client;

def client_type(request):
    client = request.META.get('HTTP_X_CLIENT_TYPE')
    if not client:
        return False
    return client;


def client_agent(request):
    client = request.META.get('HTTP_USER_AGENT')
    if not client:
        return False
    return client;

def client_type(request):
    client = request.META.get('HTTP_X_CLIENT_TYPE')
    if not client:
        return False
    return client;

def client_timezone(request):
    client = request.META.get('HTTP_X_CLIENT_TIMEZONE')
    if not client:
        return False
    return client;

def client_country(request):
    client = request.META.get('HTTP_X_CLIENT_COUNTRY')
    if not client:
        return False
    return client;

def client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

def create_client(request):
    serializer = ClientInfoSerializer(data={
    "unique_token" : client(request),
    "device_type" : client_type(request),
    "ip" : client_ip(request),
    "user_agent" : client_agent(request),
    "timezone" : client_timezone(request),
    "country" : client_country(request)
    })
    if serializer.is_valid():
        client_info = serializer.save()
        return client_info
    else:
        return None;

#generate token when login
def auth_token(user,request):
    client_info = create_client(request)
    print(client_info)
    if client_info:
        token = jwt_encode_handler(jwt_payload_handler(user))
        print(token)
        serializer = AuthTokenSerializer(data={
        "user" : user.id,
        "client_info" : client_info.id,
        "apiKey" : token,
        })
        if serializer.is_valid():
            authToken = serializer.save()
            role = UserRoles.objects.get(user_id=user.id)
            return {"token": token,"role":role.role_id.role_name}
    return None


#check user is authenticated
def auth_user(email,pwd):
   try:
       user = User.objects.get(username = email)
   except:
       user = None
       pass
   if user:
       try:
           user = User.objects.get(username = email,is_active=True)
           is_match = user.check_password(pwd)
           if is_match:
               return {"user":user,"status":False}
       except:
           return {"status":True}
       
   return None;

#check surveyor is authenticated
def auth_surveyor(ph,pwd):
   try:
       user = UserProfile.objects.get(phone_no = ph)
       print(user)  
   except:
       user = None
       pass
   if user is None:
       return {"status":True}
   else: 
       try:
           user = UserProfile.objects.get(phone_no = ph,otp=pwd,is_active=True)
           user = User.objects.get(id=user.user_id_id)
           return {"user":user,"status":False}
       except:
           return {"status":True}    
   return None;

def get_translations(lang_code):
    if lang_code:
        return lang_code;
    return None;

def get_lat_lang(address):
    g = geocoder.google(address)
    return g.latlng;

def get_geo_info(address):
    g = geocoder.google(address)
    return g;


# def generate_qr(url):
#     url = pyqrcode.create(url)
#     return url

# def generate_qr_file():
#     # import os
#     # data = "config/media/qrcode/"+str(uuid.uuid4())+".png"

#     import os

#     file_path = "config/media/qrcode/"+str(uuid.uuid4())+".png"
#     directory = os.path.dirname(file_path)
    
#     if not os.path.exists(directory):
#         os.makedirs(directory)
#     return file_path
#     # subdir = "config/media/qrcode/"
#     # filename = str(uuid.uuid4())+".png"
#     # filepath = os.path.join(subdir, filename)

#     # # create your subdirectory
#     # os.mkdir(os.path.join('app/',subdir))

#     # # create an empty file.
#     # try:
#     #     f = open(filepath, 'w')
#     #     f.close()
#     # except IOError:
#     #     print("Wrong path provided")
