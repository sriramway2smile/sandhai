from .models import *
from rest_framework import serializers
from rest_framework.validators import *
import json
from versatileimagefield.serializers import VersatileImageFieldSerializer
import django.core
import datetime
from datetime import timedelta
import moment

class UserSerializer(serializers.ModelSerializer):
    user_profile = serializers.SerializerMethodField('profile')
    def profile(self, data):
        data = UserProfile.objects.get(user_id=data.id)
        location = data.location_id.address
        data = django.core.serializers.serialize('json', [data, ])
        struct = json.loads(data)
        struct[0]['fields']['location'] = location
        if data:
            return struct[0]['fields']
        else:
            return {}

    class Meta:
        model = User
        fields = ('id', 'username','user_profile','is_active','email')
        extra_kwargs = {
            'first_name': {'required': True},
            'password':{'required':False},
            # 'created_by': {'default': serializers.CurrentUserDefault()},'updated_by': {'default': serializers.CurrentUserDefault()}
            }
  

class UserRoleSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserRoles
        fields = '__all__'




class ClientInfoSerializer(serializers.ModelSerializer):

    class Meta:
        model = ApiClientInfo
        fields = '__all__'


class AuthTokenSerializer(serializers.ModelSerializer):

    class Meta:
        model = AuthToken
        fields = '__all__'



class UploadSerializer(serializers.ModelSerializer):
    """Serializes Person instances"""
    ImageField = VersatileImageFieldSerializer(
        sizes=[
            ('full_size', 'url'),
            ('thumbnail', 'thumbnail__100x100'),
            ('medium_square_crop', 'crop__400x400'),
            ('small_square_crop', 'crop__50x50')
        ]
    )

    class Meta:
        model = Upload
        fields = '__all__'

class LocDevSerializer(serializers.ModelSerializer):

    class Meta:
        model = Locations
        fields = '__all__'


class UserDevProerializer(serializers.ModelSerializer):

    class Meta:
        model = UserProfile
        fields = '__all__'

class LocationSerializer(serializers.ModelSerializer):

    class Meta:
        model = Locations
        fields = '__all__'
        extra_kwargs = {'country': {'required': True},
                        "address": {'required': True},
                        'created_by': {'default': serializers.CurrentUserDefault()},'updated_by': {'default': serializers.CurrentUserDefault()}}

class RolesSerializer(serializers.ModelSerializer):

    class Meta:
        model = Roles
        fields = '__all__'
        extra_kwargs = {'role_name': {'required': True},
                        "role_desc": {'required': True}}


class UserProfileSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserProfile
        fields = '__all__'


class LanguageSerializer(serializers.ModelSerializer):

    class Meta:
        model = Language
        fields = '__all__'

class PageSerializer(serializers.ModelSerializer):
    # page_id = ContentSerializer()
    
    class Meta:
        model = Page
        fields = '__all__'

class ContentSerializer(serializers.ModelSerializer):
    page_id =  PageSerializer()
    language_id=LanguageSerializer()
    class Meta:
        model = Content
        fields = '__all__'


class FarmerSerializer(serializers.ModelSerializer):
    user_id = UserSerializer()

    class Meta:
        model = Farmer
        fields = '__all__'

class LandLocationvalserializer(serializers.ModelSerializer):

    class Meta:
        model = LandLocation
        fields = '__all__'


class LandLocationSerializer(serializers.ModelSerializer):
    landlocation = serializers.SerializerMethodField('land')
    farmer_id = FarmerSerializer()
    user_id= UserSerializer()
    def land(self, data):
        
        crop = None
        if(data):
            try:
                crop = CropRegistration.objects.get(land_id=data.id,is_active=True)
            except:
                pass;
            if crop is None:
                crop=""
                crop_det = ""
            else: 
                print(crop.master_crop_id)
                crop_det = Crop.objects.get(id=crop.master_crop_id_id)
                crop = django.core.serializers.serialize('json',[crop])
                crop = json.loads(crop)
                
                crop_det = django.core.serializers.serialize('json',[crop_det])
                crop_det = json.loads(crop_det)
            data = LandLocation.objects.filter(land_id=data.id).order_by('pk')
            data = django.core.serializers.serialize('json',data,fields=('lat', 'lon','is_active','order'))
            struct = json.loads(data)
            if data:
                return {"data":struct,"crop":crop,"crop_det":crop_det}
            else:
                return {}
        else:
            return {}

    class Meta:
        model = LandRegistration
        fields = '__all__'


class CropSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Crop
        fields = '__all__'

class TempCropSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = TempCropImage
        fields = '__all__'

class CropregSerializer(serializers.ModelSerializer):
    data = serializers.SerializerMethodField('crop_img')
    land_id = LandLocationSerializer()
    master_crop_id = CropSerializer()
    
    def crop_img(self, data):
        
        if(data):

            try:
                img = TempCropImage.objects.filter(crop_id=data.id)
                img = django.core.serializers.serialize('json',img)
                img = json.loads(img)
                print(img)
                return {"img":img}
            except:
                pass;



    class Meta:
        model = CropRegistration
        fields = '__all__'

class PastCropSerializer(serializers.ModelSerializer):
    land_id = LandLocationSerializer()
    data = serializers.SerializerMethodField('past_crop_data')
    # past_crop = CropSerializer()
    def past_crop_data(self, data):
        
        if(data):
            past_data=[]
            try:
                past_crop = CropRegistration.objects.filter(land_id=data.land_id,is_active=False)
                print(past_crop)
                for past_crop_data in past_crop:
                    print("heol")
                    print(past_crop_data.land_id_id)
                    past_crop = CropRegistration.objects.filter(land_id=past_crop_data.land_id_id,is_active=False)

                    past_crop = django.core.serializers.serialize('json',[past_crop_data])
                    past_crop = json.loads(past_crop)
                    past_data.append(past_crop[0])
                    print(past_crop)
                return past_data
            except:
                pass;

    class Meta:
        model = FarmerPastCrop
        fields = '__all__'

