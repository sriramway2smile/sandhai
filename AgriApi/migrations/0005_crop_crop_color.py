# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2018-04-07 10:22
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('AgriApi', '0004_crop_tamil_crop_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='crop',
            name='crop_color',
            field=models.CharField(max_length=1500, null=True),
        ),
    ]
