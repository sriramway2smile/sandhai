from rest_framework.response import Response
from django.http import HttpResponse,HttpResponseRedirect
from .models import *
from .serializer import *
from . import json
import io
from rest_framework.viewsets import ModelViewSet
from . import helpers
from rest_framework.decorators import api_view, permission_classes
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from django.db import transaction
from rest_framework import generics
from rest_framework_jwt.settings import api_settings
from rest_framework.permissions import AllowAny
jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
from django.db.models import Q


# Image Upload function

class UploadImages(generics.ListCreateAPIView):
    queryset = Upload.objects.all()
    serializer_class = UploadSerializer
    permission_classes = (AllowAny,)
    def create(self, serializer):
        data = [];
        for file in self.request.data.getlist('pic[]'):
            new = Upload.objects.create(ImageField=file)
            url = new.ImageField
            data.append({ 'url' : str(url),'id':new.id})
        return json.Response(data)

