from rest_framework_jwt.views import obtain_jwt_token,refresh_jwt_token,verify_jwt_token
from . import views
from . import api
from django.conf.urls import include, url

urlpatterns = [
    
    #auth token to login
    url(r'^auth/', obtain_jwt_token),
    url(r'^auth-refresh/', refresh_jwt_token),
    url(r'^auth-verify/', verify_jwt_token),
    url(r'^login/',views.login),
    url(r'^userlogin/',views.user_login),
    # user update paytm details
    url(r'^paytm_update/',views.update_paytm),


    # check requested login is from authenticated user
    url(r'^auth_user/',views.check_user),
    url(r'^sms_push/',views.sms_push),

    # assign roles from super admin
    url(r'^dev-roles/',views.create_roles),
    #register surveyor 
    url(r'^register/',views.register),
    # super admin password update
    url(r'^update_password_profile/$', views.update_password_profile),
    #list view of user (surveyor)
    url(r'^users/$', views.Users.as_view()),
    
    url(r'^buyer/$', views.buyer.as_view()),
    #image upload api 
    url(r'^upload/', api.UploadImages.as_view()),
    # get particular user's profile info
    url(r'^profile/$', views.user_profile_info),
    # update particular user's profile info
    url(r'^profile_update/$', views.update_profile),
    # forgot password link generated
    url(r'^forgot/$', views.forgotpassword),
    # update password link generated
    url(r'^update_password/$', views.update_password),
    url(r'^reset_pwd/',views.updatepassword),
    # Content management content View
    url(r'^content_update/$', views.contentupdate.as_view()),
    # language master details
    url(r'^language/$', views.language_master.as_view()),
    #page dettails
    url(r'^page/$', views.page_master.as_view()),
    #content update
    url(r'^content_update/(?P<pk>[0-9]+)/$', views.page_content_update.as_view()),
    # delete content
    url(r'^del_content/(?P<pk>[0-9]+)/$', views.page_content_delete),
    #block user
    url(r'^block_user/(?P<pk>[0-9]+)/$', views.block_user),
    #reset otp to the particular user/farmer
    url(r'^reset_otp/(?P<pk>[0-9]+)/$', views.reset_otp),
    #Add farmer details and get data
    url(r'^add_farmer/$', views.add_farmer.as_view()),
    # App - load land details in map 
    url(r'^farmers_land_view/$', views.farmers_land_view),
    #single land view of farmer
    url(r'^land_view/(?P<pk>[0-9]+)/$', views.edit_land_details.as_view()),
    #load all farmers 
    url(r'^load_farmer/(?P<pk>[0-9]+)/$', views.load_farmer),
    # register land for farmers
    url(r'^land_registration/$', views.land_registration.as_view()),
    url(r'^land_filter/$', views.land_filter.as_view()),
    #total search for land
    url(r'^search_list/$',views.search_list),
    #displays crop master datas
    url(r'^crop_master/$', views.crop_master.as_view()),
    #update crop master data
    url(r'^crop_master/(?P<pk>[0-9]+)/$', views.crop_master_update.as_view()),
    # add crop 
    url(r'^add_crop/$', views.add_crop.as_view()),
    # display single crop data
    url(r'^add_crop/(?P<pk>[0-9]+)/$', views.edit_crop_details.as_view()),
    #add past crop 
    url(r'^past_crop/$', views.past_crop.as_view()),
    # reports generation
    url(r'^report/$',views.report),
    # report download csv
    url(r'^csv/$',views.csv),
    # Dev's use - crop,land import func
    url(r'^csv_upload/$',views.excel_upload.as_view()),

    # remove image from crop
    url(r'^remove_image/$',views.remove_image),

    url(r'^crop_update/$',views.crop_update),

    url(r'^get_land/$', views.get_land),
    url(r'^get_surveyor_land/$', views.get_surveyor_land),

    url(r'^buyer_list/$',views.buyer_list),
    url(r'^get_count/$',views.get_crop_count),

    url(r'^captcha_view/$',views.captcha_view.as_view()),

    url(r'^captcha_verify/$',views.captcha_verify.as_view()),

    url(r'^contact_farmer/$',views.contact_farmer.as_view()),

]


