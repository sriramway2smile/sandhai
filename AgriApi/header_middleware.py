from django.utils import timezone
from django.http import HttpResponse
from django.http import HttpResponseForbidden
from . import json
from . import helpers
from . import models
import re

class HeaderMiddleware(object):

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        non_auth_urls = helpers.expect_urls()
        non_auth_rel_urls = helpers.relative_urls()
        url_rel = False
        for url in non_auth_rel_urls:
            url = re.compile(url)
            if url.match(request.path):
                url_rel = True
        # print(request.path)
        if(request.path!='/api/login/'):

            if (helpers.header(request)):
                
                key = helpers.header(request);
                if(key != 'null'):
                    try:
                        print(key)
                        token_val = models.AuthToken.objects.get(apiKey=key)
                        print(token_val.user_id)
                        token_val.last_used_ts=timezone.now()
                        token_val.save()
                        user = models.User.objects.get(pk=token_val.user_id,is_active=True)
                        print(user)
                
                    except:
                        return json.Response('User Blocked,Please contact Admin', False, 401)
            
        if  url_rel == False:
            if request.path not in non_auth_urls:
                    if helpers.header(request) == False or helpers.client(request) == False \
                            or helpers.client_timezone(request) == False \
                            or helpers.client_country(request) == False:
                        return json.Response('401 User', False, 401)
                    
        
                    # try:
                    #     key = helpers.header(request);
                    #     token = models.AuthToken.objects.get(apiKey=key);
                    #     token.last_used_ts=timezone.now()
                    #     token.save()
                    # except:
                    #     return json.Response('401 User', False, 401)
                    # if(token.client_info.unique_token != helpers.client(request)):
                    #     return json.Response('401 User', False, 401)

        return self.get_response(request)

    # def process_exception(self, request, exception):
    #     return json.Response('Server Function Error', False, 500)

